#include "MainApplication.h"

MainApplication::MainApplication() :D3DApp()
{
	// Init Window

	SetWindowSize(640, 480);
	SetWindowClassName(L"TutorialWindowClass");
	SetWindowName(L"TimotheeLECORRE-16026323");
}

void MainApplication::Initialise()
{
	// Init Cameras

	mMainCamera = new Camera(Camera::LOOK_AT);

	mCurrentCamera = mMainCamera;

	mLeftCamera = new Camera(Camera::LOOK_AT);
	mLeftCamera->SetEye(-6, 2, 0);
	mLeftCamera->SetAt(0, 0, 0);
	mLeftCamera->SetUp(0, 1, 0);

	mRightCamera = new Camera(Camera::LOOK_AT);
	mRightCamera->SetEye(6, -2, 0);
	mRightCamera->SetAt(0, 0, 0);
	mRightCamera->SetUp(0, 1, 0);

	mFPSCamera = new Camera(Camera::LOOK_TO);
	mFPSCamera->SetAt(0, 0, -1);

	ResourceManager::CreateTerrain(L"terrain", 5, 5, L"texture.dds","HeightmapExample.raw", 513, 513, 15);

	ResourceManager::CreateGeometry(L"craft", "simpleCube.xml", Geometry::XML, L"texture.dds");
	ResourceManager::CreateGeometry(L"askCube", "simpleCube.xml", Geometry::XML, L"block.dds");

	ResourceManager::CreateGeometry(L"RedCar", "car.obj", Geometry::OBJ, L"redCar.dds");
	ResourceManager::CreateGeometry(L"BlueCar", "car.obj", Geometry::OBJ, L"blueCar.dds");

	mMainShader = ResourceManager::LoadShader("DX11 Framework.fx");

	/*crateGo = new GameObject(ResourceManager::GetGeometry(L"craft"), mMainShader, XMFLOAT3(-2, 0, 0), 
		XMFLOAT3(0, 0, 0), XMFLOAT3(0.5f, 0.5f, 0.5f));
	crateGo2 = new GameObject(ResourceManager::GetGeometry(L"askCube"), mMainShader, XMFLOAT3(2, 1, 0), XMFLOAT3(0, 1, 0), XMFLOAT3(0.5f, 0.5f, 0.5f));
*/
	//ObjectManager::AddGameObject(L"crate", crateGo);
	//ObjectManager::AddGameObject(L"askCube", crateGo2);
	//cubeGo = new GameObject(mCube2, mTestShader, XMFLOAT3(0, 0, -6), XMFLOAT3(0, 0, 0), XMFLOAT3(0.5f, 0.5f, 0.5f));
	//terrainGo = new GameObject(terrain, mTestShader, XMFLOAT3(0, -5, 0), XMFLOAT3(0, 0, 0));

	mCarRed = new GameObject(ResourceManager::GetGeometry(L"RedCar")
		, mMainShader, XMFLOAT3(-50,-50, 100), XMFLOAT3(0, 0, 0), XMFLOAT3(0.5f, 0.5f, 0.5f));
	mCarBlue = new GameObject(ResourceManager::GetGeometry(L"BlueCar")
		, mMainShader, XMFLOAT3(50,-50, 100), XMFLOAT3(0, 0, 0), XMFLOAT3(0.5f, 0.5f, 0.5f));

	ObjectManager::AddGameObject(L"RedCar",mCarRed);
	ObjectManager::AddGameObject(L"BlueCar",mCarBlue);

	path.push_back(XMFLOAT3(-2, 0, 0));
	path.push_back(XMFLOAT3(2, 1, 0));
	path.push_back(XMFLOAT3(0, 0, -6));
	path.push_back(XMFLOAT3(6, -2, 0));

	currentPathID = 0;
	progress = 0;

	ClearLights();

	lights.push_back(LightManager::
		CreateDiffuseLighting(XMFLOAT3(0.25f, 0.5f, -1.0f), XMFLOAT4(0.8f, 0.5f, 0.5f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)));
	lights.push_back(LightManager::CreateAmbiantLighting(XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f)));
	lights.push_back(LightManager::CreateSpecularLighting(XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f), XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f), 10.0f));
}

void MainApplication::Cleanup()
{
	if (mMainCamera) delete mMainCamera;
	if (mLeftCamera) delete mLeftCamera;
	if (mRightCamera) delete mRightCamera;
	if (mFPSCamera) delete mFPSCamera;

	path.clear();
}

void MainApplication::Update(float elapsedTime)
{
	// Switch Cameras

	if (GetAsyncKeyState(VK_NUMPAD0))
		mCurrentCamera = mMainCamera;
	if (GetAsyncKeyState(VK_NUMPAD1))
		mCurrentCamera = mLeftCamera;
	if (GetAsyncKeyState(VK_NUMPAD2))
		mCurrentCamera = mRightCamera;
	if (GetAsyncKeyState(VK_NUMPAD3))
		mCurrentCamera = mFPSCamera;

	// Camera Movement
	MoveCamera();

	if (mCurrentCamera == mRightCamera)
	{
		progress = elapsedTime;

		XMVECTOR eye = mCurrentCamera->GetEye();
		XMVECTOR next = XMLoadFloat3(&path[currentPathID]);

		XMFLOAT3 lenght;
		XMStoreFloat3(&lenght, XMVector3Length(eye - next));

		if (lenght.x >= 0.8f)
		{
			XMVECTOR test = XMVectorLerp(mCurrentCamera->GetEye(), XMLoadFloat3(&path[currentPathID]), progress);

			XMFLOAT3 test2;
			XMStoreFloat3(&test2, test);

			mCurrentCamera->SetEye(test);
		}
		else
		{
			currentPathID = (currentPathID + 1) % path.size();
		}
	}

	/*crateGo->Rotate(elapsedTime, 0, 0);
	crateGo2->Rotate(elapsedTime, 0, 0);*/
	//cubeGo->Rotate(elapsedTime, 0, 0);
}

void MainApplication::Draw()
{

}

void MainApplication::MoveCamera()
{
	XMVECTOR vector = mCurrentCamera->GetEye() - mCurrentCamera->GetAt();
	vector = XMVector3Normalize(vector);

	if (!mKeyPressed)
	{
		if (GetAsyncKeyState(VK_SUBTRACT))
		{
			vector = mCurrentCamera->GetEye() + vector;

			mCurrentCamera->SetEye(vector);

			mKeyPressed = true;
		}
		else if (GetAsyncKeyState(VK_ADD))
		{
			vector = mCurrentCamera->GetEye() - vector;

			XMFLOAT3 pos;
			XMStoreFloat3(&pos, vector);

			XMFLOAT3 at;
			XMStoreFloat3(&at, mCurrentCamera->GetAt());

			if (at.x != pos.x || at.y != pos.y || at.z != pos.z)
			{
				mCurrentCamera->SetEye(vector);
			}

			mKeyPressed = true;
		}
		else if (GetAsyncKeyState(VK_LEFT))
		{
			mCurrentCamera->Rotate(0, -0.05f, 0);

			mKeyPressed = true;
		}
		else if (GetAsyncKeyState(VK_RIGHT))
		{
			mCurrentCamera->Rotate(0, 0.05f, 0);

			mKeyPressed = true;
		}
	}
	else if (mCurrentCamera->GetMode() == Camera::eMode::LOOK_TO)
	{
		XMVECTOR vector = mCurrentCamera->GetAt();

		if (!mKeyPressed)
		{
			if (GetAsyncKeyState(VK_SUBTRACT))
			{
				vector = mCurrentCamera->GetEye() - vector;

				mCurrentCamera->SetEye(vector);

				mKeyPressed = true;
			}
			else if (GetAsyncKeyState(VK_ADD))
			{
				vector = mCurrentCamera->GetEye() + vector;

				mCurrentCamera->SetEye(vector);

				mKeyPressed = true;
			}
			else if (GetAsyncKeyState(VK_LEFT))
			{
				mCurrentCamera->SetEye(mCurrentCamera->GetEye() + mCurrentCamera->GetRight() * (-0.05f));

				mKeyPressed = true;
			}
			else if (GetAsyncKeyState(VK_RIGHT))
			{
				mCurrentCamera->SetEye(mCurrentCamera->GetEye() + mCurrentCamera->GetRight() * 0.05f);

				mKeyPressed = true;
			}
		}
	}
}

MainApplication::~MainApplication()
{
}
