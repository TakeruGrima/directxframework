
#ifndef UTIL_H
#define UTIL_H

#include <directxcolors.h>

bool Float3IsEqual(DirectX::XMFLOAT3 u, DirectX::XMFLOAT3 v);

#endif
