#pragma once
#include "D3DApp.h"

#include "../DirectXFramework/GraphicsEngine/Geometry.h"
#include "../DirectXFramework/GraphicsEngine/Shader.h"
#include "../DirectXFramework/GraphicsEngine/GameObject.h"
#include "../DirectXFramework/GraphicsEngine/Terrain.h"

class MainApplication : public D3DApp
{
	bool mKeyPressed = false;
	float mTime = 0;

	Camera * mMainCamera;
	Camera * mLeftCamera;
	Camera * mRightCamera;
	Camera * mFPSCamera;

	GameObject * crateGo;
	GameObject * crateGo2;
	GameObject * cubeGo;
	GameObject * terrainGo;
	//Geometry * mCube1;
	//Geometry * mCube2;
	//Terrain * terrain;
	Shader * mMainShader;

	//Geometry * mRedCarMesh;
	//Geometry * mBlueCarMesh;
	GameObject * mCarRed;
	GameObject * mCarBlue;

	std::vector<XMFLOAT3> path;
	int currentPathID;
	float progress;
protected:
	void Initialise();
	void Cleanup();
	void Update(float elapsedTime);
	void Draw();
private:
	void MoveCamera();
public:
	MainApplication();
	~MainApplication();
};

