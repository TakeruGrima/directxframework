#pragma once

#include <vector>
#include <map>
#include <iterator>

using namespace std;

template<class T>
class PointerDictionary
{
	map < wstring, T *> dic;
public:
	T * operator[](wstring name)
	{
		return dic[name];
	}

	auto Begin() { return dic.begin(); }
	auto End() { return dic.end(); }

	void Add(wstring name, T * item)
	{
		dic.insert(pair<wstring, T *>(name, item));
	}

	bool IfExist(wstring name)
	{
		if (dic.size() == 0) return false;

		if (dic.find(name) == dic.end()) {
			return false;
		}
		else {
			return true;
		}
	}

	void Clear()
	{
		dic.clear();
	}

	void DeleteAllElements()
	{
		auto it = dic.begin();
		// Iterate through the dictionary
		while (it != dic.end())
		{
			T * element = it->second;

			it = dic.erase(it);
			delete element;
		}
	}
};
