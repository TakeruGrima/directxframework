#include "D3DApp.h"

using namespace DirectX;

D3DApp::D3DApp()
{
	mHInst = nullptr;
	mHWnd = nullptr;
	mDriverType = D3D_DRIVER_TYPE_NULL;
	mFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	mPSwapChain = nullptr;
	mPRenderTargetView = nullptr;
	mCurrentCamera = nullptr;
}


D3DApp::~D3DApp()
{
	CleanupAll();
}

void D3DApp::ClearLights()
{
	for (size_t i = 0; i < lights.size(); i++)
	{
		delete lights[i];
	}

	lights.clear();
}

HRESULT D3DApp::InitBase(HINSTANCE hInstance, int nCmdShow)
{
	if (FAILED(InitWindow(hInstance, nCmdShow)))
	{
		return E_FAIL;
	}

	RECT rc;
	GetClientRect(mHWnd, &rc);
	mWindowWidth = rc.right - rc.left;
	mWindowHeight = rc.bottom - rc.top;

	if (FAILED(InitDevice()))
	{
		CleanupAll();

		return E_FAIL;
	}

	Initialise();

	if (mCurrentCamera == nullptr)
	{
		mCurrentCamera = new Camera(Camera::LOOK_AT);
	}

	return S_OK;
}

HRESULT D3DApp::InitDevice()
{
	HRESULT hr = S_OK;

	UINT createDeviceFlags = 0;

#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};

	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = mWindowWidth;
	sd.BufferDesc.Height = mWindowHeight;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;// how many bit / pixel
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = mHWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	ID3D11Device * pd3dDevice = nullptr;
	ID3D11DeviceContext * pImmediateContext = nullptr;
	ID3D11Buffer * pConstantBuffer = nullptr;

	//Create device
	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		mDriverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(nullptr, mDriverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &mPSwapChain, &pd3dDevice, &mFeatureLevel, &pImmediateContext);
		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
		return hr;

	// setup depth stencil here

	// Define buffer
	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width = mWindowWidth;
	depthStencilDesc.Height = mWindowHeight;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;// 24 for depth and 8 for stencil
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	//create depthStencil buffer
	pd3dDevice->CreateTexture2D(&depthStencilDesc, nullptr, &mDepthStencilBuffer);
	// create depthStencil view
	pd3dDevice->CreateDepthStencilView(mDepthStencilBuffer, nullptr, &mDepthStencilView);

	// Create a render target view
	ID3D11Texture2D* pBackBuffer = nullptr;
	hr = mPSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

	if (FAILED(hr))
		return hr;

	hr = pd3dDevice->CreateRenderTargetView(pBackBuffer, nullptr, &mPRenderTargetView);
	pBackBuffer->Release();

	if (FAILED(hr))
		return hr;

	// replace null_ptr by _depthStencilView because we have one
	pImmediateContext->OMSetRenderTargets(1, &mPRenderTargetView, mDepthStencilView);

	// Setup the viewport
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)mWindowWidth;
	vp.Height = (FLOAT)mWindowHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	pImmediateContext->RSSetViewports(1, &vp);

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	pd3dDevice->CreateSamplerState(&sampDesc, &mPSamplerLinear);
	pImmediateContext->PSSetSamplers(0, 1, &mPSamplerLinear);

	// Set primitive topology
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = pd3dDevice->CreateBuffer(&bd, nullptr, &pConstantBuffer);

	//Creating a rasterizer state 

	D3D11_RASTERIZER_DESC wfdesc;
	ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
	wfdesc.FillMode = D3D11_FILL_WIREFRAME;
	wfdesc.CullMode = D3D11_CULL_NONE;
	hr = pd3dDevice->CreateRasterizerState(&wfdesc, &mWireFrame);

	ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
	wfdesc.FillMode = D3D11_FILL_SOLID;
	wfdesc.CullMode = D3D11_CULL_BACK;
	hr = pd3dDevice->CreateRasterizerState(&wfdesc, &mSolid);

	mCurrentRS = mSolid;

	pImmediateContext->RSSetState(mCurrentRS);

	if (FAILED(hr))
		return hr;

	// init default lighting
	lights.push_back(LightManager::
		CreateDiffuseLighting(XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)));
	lights.push_back(LightManager::CreateAmbiantLighting(XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f)));
	lights.push_back(LightManager::CreateSpecularLighting(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), 1.0f));

	Graphics::Set3DDevice(pd3dDevice);
	Graphics::SetDeviceContext(pImmediateContext);
	Graphics::SetConstantBuffer(pConstantBuffer);

	return S_OK;
}

void D3DApp::CleanupAll()
{
	// free window fields
	mClassName = nullptr;
	mWindowName = nullptr;

	Graphics * toDelete = Graphics::Get();
	ResourceManager::Clear();
	ObjectManager::Clear();

	ClearLights();

	if (toDelete) delete toDelete;

	if (mPRenderTargetView) mPRenderTargetView->Release();
	if (mPSwapChain) mPSwapChain->Release();

	if (mDepthStencilView) mDepthStencilView->Release();
	if (mDepthStencilBuffer) mDepthStencilBuffer->Release();
	if (mSolid) mSolid->Release();
	if (mWireFrame) mWireFrame->Release();
	if (mPSamplerLinear) mPSamplerLinear->Release();

	Cleanup();
}

void D3DApp::UpdateBase()
{
	// Update our time
	static float t = 0.0f;

	if (mDriverType == D3D_DRIVER_TYPE_REFERENCE)
	{
		t += (float)XM_PI * 0.0125f;
	}
	else
	{
		static DWORD dwTimeStart = 0;
		DWORD dwTimeCur = GetTickCount();

		if (dwTimeStart == 0)
			dwTimeStart = dwTimeCur;

		t = (dwTimeCur - dwTimeStart) / 1000.0f - mPrevTime;

		mPrevTime += t;
	}

	if (GetAsyncKeyState(0x45))
	{
		if (mCurrentRS == mWireFrame)
			mCurrentRS = mSolid;
		else
			mCurrentRS = mWireFrame;

		Graphics::GetDeviceContext()->RSSetState(mCurrentRS);
	}

	Update(t);

	ObjectManager::UpdateObjects();

	if (mCurrentCamera != nullptr)
		mCurrentCamera->Update();

}

void D3DApp::DrawBase()
{
	//
	// Clear the back buffer
	//
	Graphics::GetDeviceContext()->ClearRenderTargetView(mPRenderTargetView, mClearColor);

	// clear ClearDepthStencilView
	Graphics::GetDeviceContext()->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//Draw Something
	//Draw();
	ObjectManager::DrawObjects(mCurrentCamera,lights);

	//
	// Present our back buffer to our front buffer
	//
	mPSwapChain->Present(0, 0);
}

#pragma region Window methods

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

HRESULT D3DApp::InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_TUTORIAL1);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = mClassName;
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
	if (!RegisterClassEx(&wcex))
		return E_FAIL;

	// Create window
	mHInst = hInstance;
	RECT rc = { 0, 0, Graphics::GetWindowWidth(), Graphics::GetWindowHeight() };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	mHWnd = CreateWindow(mClassName, mWindowName, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance,
		nullptr);
	if (!mHWnd)
		return E_FAIL;

	ShowWindow(mHWnd, nCmdShow);

	return S_OK;
}

void D3DApp::SetWindowClassName(wchar_t * className)
{
	mClassName = className;
}

void D3DApp::SetWindowName(wchar_t * windowName)
{
	mWindowName = windowName;
}

void D3DApp::SetWindowSize(int width, int height)
{
	Graphics::SetWindowSize(width, height);
}

#pragma endregion

void D3DApp::SetClearColor255(int r, int g, int b, int a)
{
	SetClearColor(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
}

void D3DApp::SetClearColor(float r, float g, float b, float a)
{
	mClearColor[0] = r;
	mClearColor[1] = g;
	mClearColor[2] = b;
	mClearColor[3] = a;
}