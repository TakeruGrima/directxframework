#include "Light.h"

using namespace DirectX;

Light::Light()
{
	mType = NONE;

	mLightDirection = nullptr;
	mDiffuseMaterial = nullptr;
	mDiffuseLight = nullptr;

	mAmbientLight = nullptr;
	mAmbientMaterial = nullptr;

	mSpecularLight = nullptr;
	mSpecularMtrl = nullptr;
	mSpecularPower = 0;
}

Light::Light(XMFLOAT3 & lightDirection, XMFLOAT4 & diffuseMaterial, XMFLOAT4 & diffuseLight):Light()
{
	mType = DIFFUSE;

	mLightDirection = new XMFLOAT3(lightDirection);
	mDiffuseMaterial = new XMFLOAT4(diffuseMaterial);
	mDiffuseLight = new XMFLOAT4(diffuseLight);
}

Light::Light(XMFLOAT4 & ambiantMaterial, XMFLOAT4 & ambiantLight):Light()
{
	mType = AMBIANT;

	mAmbientLight = new XMFLOAT4(ambiantLight);
	mAmbientMaterial = new XMFLOAT4(ambiantMaterial);
}

Light::Light(XMFLOAT4 & specularMaterial, XMFLOAT4 & specularLight, const float & specularPower):Light()
{
	mType = SPECULAR;

	mSpecularLight = new XMFLOAT4(specularLight);
	mSpecularMtrl = new XMFLOAT4(specularMaterial);
	mSpecularPower = specularPower;
}

void Light::ApplyLighting(ConstantBuffer * cb, Camera * camera)
{
	switch (mType)
	{
	case Light::DIFFUSE:
		cb->DiffuseLight = *mDiffuseLight;
		cb->DiffuseMtrl = *mDiffuseMaterial;
		cb->LightVecW = *mLightDirection;
		break;
	case Light::AMBIANT:
		cb->AmbientLight = *mAmbientLight;
		cb->AmbientMtrl = *mAmbientMaterial;
		break;
	case Light::SPECULAR:
		cb->SpecularLight = *mSpecularLight;
		cb->SpecularMtrl = *mSpecularMtrl;
		cb->SpecularPower = mSpecularPower;
		XMStoreFloat3(&cb->EyePosW, camera->GetEye());
		break;
	default:
		break;
	}
}

Light::~Light()
{
	switch (mType)
	{
	case Light::DIFFUSE:
		delete mDiffuseLight;
		delete mDiffuseMaterial;
		delete mLightDirection;
		break;
	case Light::AMBIANT:
		delete mAmbientLight;
		delete mAmbientMaterial;
		break;
	case Light::SPECULAR:
		delete mSpecularLight;
		delete mSpecularMtrl;
		break;
	}
}
