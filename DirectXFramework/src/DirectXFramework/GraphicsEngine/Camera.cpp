#include "Camera.h"

using namespace DirectX;

Camera::Camera(eMode mode)
{
	mUpdateCamera = false;
	mMode = mode;

	// Initialize the view matrix
	XMVECTOR Eye = XMVectorSet(0.0f, 0.0f, -3.0f, 0.0f);
	XMVECTOR At;
	if (mMode == eMode::LOOK_AT)
		At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	else
		At = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);

	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMStoreFloat3(&mEye, Eye);
	XMStoreFloat3(&mAt, At);
	XMStoreFloat3(&mUp, Up);

	SetViewMatrix();

	// Initialize the projection matrix
	XMStoreFloat4x4(&mProjection, XMMatrixPerspectiveFovLH(XM_PIDIV2,
		Graphics::GetWindowWidth() / (float)Graphics::GetWindowHeight(), 0.01f, 100.0f));
}

#pragma region Set Vectors

void Camera::SetEye(XMVECTOR &eyePosition)
{
	XMFLOAT3 eye;
	XMStoreFloat3(&eye, eyePosition);

	SetEye(eye);
}

void Camera::SetEye(XMFLOAT3 &eyePosition)
{
	mEye = eyePosition;

	mUpdateCamera = true;
}

void Camera::SetEye(float x, float y, float z)
{
	SetEye(XMFLOAT3(x, y, z));
}

void Camera::SetAt(XMVECTOR &eyeDirection)
{
	XMFLOAT3 at;
	XMStoreFloat3(&at, eyeDirection);

	SetAt(at);
}

void Camera::SetAt(XMFLOAT3 &eyeDirection)
{
	mAt = eyeDirection;
	mUpdateCamera = true;
}

void Camera::SetAt(float x, float y, float z)
{
	SetAt(XMFLOAT3(x, y, z));
}

void Camera::SetUp(XMVECTOR &up)
{
	XMFLOAT3 tmp;
	XMStoreFloat3(&tmp, up);

	SetUp(tmp);
}

void Camera::SetUp(XMFLOAT3 &up)
{
	mUp = up;
	mUpdateCamera = true;
}

void Camera::SetUp(float x, float y, float z)
{
	SetUp(XMFLOAT3(x, y, z));
}

#pragma endregion

void Camera::Update()
{
	if (mUpdateCamera)
		SetViewMatrix();
}

void Camera::Rotate(float angleX, float angleY, float angleZ)
{
	if (mMode == eMode::LOOK_AT)
	{
		XMVECTOR at = GetAt();

		at -= GetEye();

		at = XMVector3Transform(at, XMMatrixRotationX(angleX) * XMMatrixRotationY(angleY) * XMMatrixRotationZ(angleZ));

		at += GetEye();

		SetAt(at);
	}
	else
	{
		XMVECTOR at = GetAt();
		at = XMVector3Transform(at, XMMatrixRotationX(angleX) * XMMatrixRotationY(angleY) * XMMatrixRotationZ(angleZ));
		SetAt(at);
	}
}

XMVECTOR Camera::GetRight()
{
	XMVECTOR forward = XMVector3Normalize(GetAt() - GetEye());

	return XMVector3Cross(forward, GetUp());
}

XMMATRIX Camera::GetViewMatrix()
{
	return XMLoadFloat4x4(&mView);
}

XMMATRIX Camera::GetProjectionMatrix()
{
	return XMLoadFloat4x4(&mProjection);
}


Camera::~Camera()
{
}

void Camera::SetViewMatrix()
{
	// Initialize the view matrix
	XMVECTOR Eye = XMLoadFloat3(&mEye);
	XMVECTOR At = XMLoadFloat3(&mAt);
	XMVECTOR Up = XMLoadFloat3(&mUp);

	switch (mMode)
	{
	case Camera::LOOK_AT:
		XMStoreFloat4x4(&mView, XMMatrixLookAtLH(Eye, At, Up));
		break;
	case Camera::LOOK_TO:
		XMStoreFloat4x4(&mView, XMMatrixLookToLH(Eye, At, Up));
		break;
	default:
		break;
	}

	mUpdateCamera = false;
}