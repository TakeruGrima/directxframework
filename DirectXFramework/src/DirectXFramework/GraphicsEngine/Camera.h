#pragma once

#include <directxcolors.h>

#include "Graphics.h"

class Camera
{
public:
	enum eMode { LOOK_AT, LOOK_TO };
private:
	bool mUpdateCamera;
	eMode mMode;

	DirectX::XMFLOAT3 mEye;
	DirectX::XMFLOAT3 mAt;
	DirectX::XMFLOAT3 mUp;

	DirectX::XMFLOAT4X4 mView;
	DirectX::XMFLOAT4X4 mProjection;
public:
	Camera(eMode mode);

	void Update();

	void SetEye(DirectX::XMVECTOR & eyePosition);
	void SetEye(DirectX::XMFLOAT3 & eyePosition);
	void SetEye(float x, float y, float z);
	void SetAt(DirectX::XMVECTOR & eyeDirection);
	void SetAt(DirectX::XMFLOAT3 & eyeDirection);
	void SetAt(float x, float y, float z);
	void SetUp(DirectX::XMVECTOR &up);
	void SetUp(DirectX::XMFLOAT3 &up);
	void SetUp(float x, float y, float z);

	DirectX::XMVECTOR GetEye() { return XMLoadFloat3(&mEye); }
	DirectX::XMVECTOR GetAt() { return XMLoadFloat3(&mAt); }
	DirectX::XMVECTOR GetUp() { return XMLoadFloat3(&mUp); }
	DirectX::XMVECTOR GetRight();

	void Rotate(float angleX, float angleY, float angleZ);

	eMode GetMode() { return mMode; }
	DirectX::XMMATRIX GetViewMatrix();
	DirectX::XMMATRIX GetProjectionMatrix();

	~Camera();
private:
	void SetViewMatrix();
};