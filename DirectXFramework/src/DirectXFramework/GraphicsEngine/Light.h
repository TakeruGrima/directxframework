#pragma once

#include <directxcolors.h>

#include "Graphics.h"
#include "Camera.h"

class Light
{
public:
	enum eType { NONE,DIFFUSE, AMBIANT, SPECULAR };
private:
	eType mType;

	// Light direction from surface (XYZ)
	DirectX::XMFLOAT3 * mLightDirection;

	// Diffuse Fields
	// Diffuse material properties (RGBA)
	DirectX::XMFLOAT4 * mDiffuseMaterial;
	// Diffuse light colour (RGBA)
	DirectX::XMFLOAT4 * mDiffuseLight;

	// Ambiant Fields
	// ambient material properties (RGBA)
	DirectX::XMFLOAT4 * mAmbientMaterial;
	// ambient light colour (RGBA)
	DirectX::XMFLOAT4 * mAmbientLight;

	// Specular Fields
	DirectX::XMFLOAT4 * mSpecularMtrl;
	DirectX::XMFLOAT4 * mSpecularLight;
	float mSpecularPower;
public:
	Light();
	Light(DirectX::XMFLOAT3 &lightDirection, DirectX::XMFLOAT4 &diffuseMaterial
		, DirectX::XMFLOAT4 &diffuseLight);
	Light(DirectX::XMFLOAT4 &ambiantMaterial, DirectX::XMFLOAT4 &ambiantLight);
	Light(DirectX::XMFLOAT4 &specularMaterial, DirectX::XMFLOAT4 &specularLight,
		const float &specularPower);

	eType GetType() { return mType; }

	void ApplyLighting(ConstantBuffer * cb,Camera * camera);

	~Light();
};

