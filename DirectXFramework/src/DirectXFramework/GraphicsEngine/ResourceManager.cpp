#include "ResourceManager.h"

using namespace std;

ResourceManager * ResourceManager::mInstance = nullptr;
const wstring ResourceManager::TextureRoot = L"Content/Textures/";
const wstring ResourceManager::HeightmapRoot = L"Content/HeightMaps/";
const string ResourceManager::GeometryRoot = "Content/Geometries/";
const string ResourceManager::MeshRoot = "Content/Objects/";
const wstring ResourceManager::ShaderRoot = L"Content/Shaders/";

ResourceManager::ResourceManager()
{
}


ID3D11ShaderResourceView * ResourceManager::LoadTexture(const wchar_t * filePath)
{
	if (filePath == nullptr) return nullptr;

	wstring stringFileName(filePath);
	wstring stringFilePath = TextureRoot + stringFileName;

	if (IfTextureExist(stringFilePath))
	{
		return GetTexture(stringFilePath);
	}

	ID3D11ShaderResourceView * texture = nullptr;

	CreateDDSTextureFromFile(Graphics::Get3DDevice(), stringFilePath.c_str(), nullptr, &texture);

	AddTexture(texture, stringFilePath);

	return texture;
}

Geometry * ResourceManager::CreateGeometry(std::wstring name,char * path, Geometry::eDataType dataType, const wchar_t * texturePath)
{
	assert(!IfGeometryExist(name));

	Geometry * geometry = new Geometry(path, dataType, texturePath);
	AddGeometry(geometry, name);

	return geometry;
}

Terrain * ResourceManager::CreateTerrain(std::wstring name, int width, int depth, const wchar_t * texturePath, string heightmapPath, int heightMapWidth, int heightMapHeight, float heightScale)
{
	assert(!IfGeometryExist(name));

	Terrain * terrain = new Terrain(width, depth, texturePath, heightmapPath, heightMapWidth, heightMapHeight, heightScale);
	AddGeometry(terrain, name);

	return terrain;
}

MeshData * ResourceManager::LoadMesh(char * fullPath)
{
	string s(fullPath);
	wstring ws = StringToWstring(fullPath);
	if (IfMeshExist(ws))
		return GetMesh(ws);
	return nullptr;
}

Shader * ResourceManager::LoadShader(string filePath)
{
	wstring stringFilePath = ShaderRoot + StringToWstring(filePath);

	if (IfShaderExist(stringFilePath))
	{
		return GetShader(stringFilePath);
	}

	Shader * shader = new Shader((WCHAR *)stringFilePath.c_str(), nullptr);

	AddShader(shader, stringFilePath);

	return shader;
}

ResourceManager::~ResourceManager()
{
	ClearTextures();
	ClearMeshs();
	ClearGeometries();
	ClearShaders();

	mInstance = nullptr;
}

wstring ResourceManager::StringToWstring(string str)
{
	wstring wstr(str.begin(), str.end());

	return wstr;
}

#pragma region Texture

bool ResourceManager::IfTextureExist(wstring textureName)
{
	return Get()->mTextures.IfExist(textureName);
}

void ResourceManager::AddTexture(ID3D11ShaderResourceView * texture, wstring textureName)
{
	Get()->mTextures.Add(textureName, texture);
}

void ResourceManager::ClearTextures()
{
	PointerDictionary<ID3D11ShaderResourceView> textures = Get()->mTextures;

	for (auto it = textures.Begin(); it != textures.End(); ++it)
	{
		if(it->second!= nullptr)
			it->second->Release();
	}

	textures.Clear();
}

#pragma endregion

#pragma region Geometries

bool ResourceManager::IfGeometryExist(wstring geometryName)
{
	PointerDictionary<Geometry> geometries = Get()->mGeometries;

	bool returnV = Get()->mGeometries.IfExist(geometryName);
	return Get()->mGeometries.IfExist(geometryName);
}

void ResourceManager::AddGeometry(Geometry * geometry, wstring geometryName)
{
	Get()->mGeometries.Add(geometryName, geometry);
}

void ResourceManager::ClearGeometries()
{
	PointerDictionary<Geometry> geometries = Get()->mGeometries;

	for (auto it = geometries.Begin(); it != geometries.End(); ++it)
	{
		delete it->second;
	}

	geometries.Clear();
}

#pragma endregion

#pragma region Mesh

bool ResourceManager::IfMeshExist(wstring meshName)
{
	return Get()->mMeshs.IfExist(meshName);
}

void ResourceManager::AddMesh(MeshData * mesh, wstring meshName)
{
	Get()->mMeshs.Add(meshName, mesh);
}

void ResourceManager::ClearMeshs()
{
	PointerDictionary<MeshData> meshs = Get()->mMeshs;

	for (auto it = meshs.Begin(); it != meshs.End(); ++it)
	{
		MeshData * mesh = it->second;
		mesh->IndexBuffer->Release();
		mesh->VertexBuffer->Release();
		delete mesh;
	}

	meshs.Clear();
}

#pragma endregion

#pragma region Shader

bool ResourceManager::IfShaderExist(std::wstring shaderName)
{
	return Get()->mShaders.IfExist(shaderName);
}

void ResourceManager::AddShader(Shader * shader, std::wstring shaderName)
{
	Get()->mShaders.Add(shaderName, shader);
}

void ResourceManager::ClearShaders()
{
	PointerDictionary<Shader> shaders = Get()->mShaders;

	for (auto it = shaders.Begin(); it != shaders.End(); ++it)
	{
		delete it->second;
	}

	shaders.Clear();
}

#pragma endregion


