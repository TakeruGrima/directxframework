#pragma once

#include <cassert>

#include "../PointerDictionary.h"
#include "GameObject.h"

class ObjectManager
{
private:
	static ObjectManager * mInstance;
	PointerDictionary<GameObject> mObjects;
public:
	ObjectManager();

	static ObjectManager * Get()
	{
		if (mInstance == nullptr)
			mInstance = new ObjectManager();

		return mInstance;
	}

	static void AddGameObject(wstring name,GameObject * object);
	static GameObject * GetGameObject(wstring name);

	static void UpdateObjects();
	static void DrawObjects(Camera * camera, std::vector<Light *> lights);

	static void Clear() { delete Get(); }

	~ObjectManager();
private:
	void ClearObjects();
	static bool IfObjectExists(wstring name);
};

