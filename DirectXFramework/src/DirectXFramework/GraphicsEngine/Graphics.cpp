#include "Graphics.h"

using namespace DirectX;

Graphics * Graphics::mInstance;

Graphics::Graphics()
{
	mPd3dDevice = nullptr;
	mPImmediateContext = nullptr;
	mPConstantBuffer = nullptr;
}

void Graphics::Set3DDevice(ID3D11Device * pd3dDevice)
{
	Get()->mPd3dDevice = pd3dDevice;
}

void Graphics::SetDeviceContext(ID3D11DeviceContext * pImmediateContext)
{
	Get()->mPImmediateContext = pImmediateContext;
}

void Graphics::SetConstantBuffer(ID3D11Buffer * pConstantBuffer)
{
	Get()->mPConstantBuffer = pConstantBuffer;
}

Graphics::~Graphics()
{
	if (mPImmediateContext) mPImmediateContext->ClearState();

	if (mPConstantBuffer) mPConstantBuffer->Release();
	if (mPImmediateContext) mPImmediateContext->Release();
	if (mPd3dDevice) mPd3dDevice->Release();
}
