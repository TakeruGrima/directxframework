#include "ObjectManager.h"

ObjectManager * ObjectManager::mInstance = nullptr;

ObjectManager::ObjectManager()
{
	
}


void ObjectManager::AddGameObject(wstring name, GameObject * object)
{
	assert(!IfObjectExists(name));

	Get()->mObjects.Add(name, object);
}

GameObject * ObjectManager::GetGameObject(wstring name)
{
	return Get()->mObjects[name];
}

void ObjectManager::UpdateObjects()
{
	PointerDictionary<GameObject> objects = Get()->mObjects;

	for (auto it = objects.Begin(); it != objects.End(); ++it)
	{
		if (it->second != nullptr)
			it->second->Update();
	}
}

void ObjectManager::DrawObjects(Camera * camera, std::vector<Light *> lights)
{
	PointerDictionary<GameObject> objects = Get()->mObjects;

	for (auto it = objects.Begin(); it != objects.End(); ++it)
	{
		if (it->second != nullptr)
			it->second->Render(camera,lights);
	}
}

void ObjectManager::ClearObjects()
{
	PointerDictionary<GameObject> objects = Get()->mObjects;

	for (auto it = objects.Begin(); it != objects.End(); ++it)
	{
		if (it->second != nullptr)
			delete it->second;
	}

	objects.Clear();
}

bool ObjectManager::IfObjectExists(wstring name)
{
	return Get()->mObjects.IfExist(name);
}


ObjectManager::~ObjectManager()
{
	ClearObjects();
	mInstance = nullptr;
}