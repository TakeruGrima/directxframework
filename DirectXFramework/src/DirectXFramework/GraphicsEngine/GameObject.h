#pragma once

#include "Geometry.h"
#include "Light.h"

class GameObject
{
	Geometry * mGeometry;
	Shader * mShader;

	XMFLOAT4X4 mWorld;

	XMFLOAT3 mPosition = XMFLOAT3(0, 0, 0);
	XMFLOAT3 mRotation = XMFLOAT3(0, 0, 0);
	XMFLOAT3 mScale = XMFLOAT3(1, 1, 1);

	XMFLOAT3 mCurrentTranslation = XMFLOAT3(0, 0, 0);

	XMMATRIX mCurrentTransfo;
	XMMATRIX mTestTransfo;
public:
	enum eSpace {LOCAL,WORLD};
public:
	GameObject(Geometry * geometry,Shader *shader, XMFLOAT3 &position = XMFLOAT3(0,0,0), 
		XMFLOAT3 &rotation = XMFLOAT3(0, 0, 0),XMFLOAT3 &scale = XMFLOAT3(1, 1, 1));

	XMFLOAT4X4 * GetWorld() { return &mWorld; }

	void Rotate(float angleX, float angleY, float angleZ, eSpace relativeSpace = eSpace::WORLD);
	void Translate(float x, float y, float z, eSpace relativeSpace = eSpace::WORLD);

	void Update();
	void Render(Camera * camera, std::vector<Light *> lights);

	~GameObject();
};

