#include "Terrain.h"

using namespace std;

Terrain::Terrain(int width, int depth, const wchar_t * texturePath, string heightmapPath, int heightMapWidth, int heightMapHeight, float heightScale) : mWidth(width), mDepth(depth)
{
	LoadHeightmap(heightmapPath, heightMapWidth, heightMapHeight, heightScale);
	LoadGeometry(texturePath);
}

void Terrain::LoadHeightmap(string heightmapPath, int heightMapWidth, int heightMapHeight, float heightScale)
{
	int sizeHeightMap = mWidth * mDepth;//heightMapWidth * heightMapHeight

	// A height for each vertex 
	std::vector<unsigned char> in(sizeHeightMap);

	// Open the file
	std::ifstream inFile;
	inFile.open(heightmapPath.c_str(), std::ios_base::binary);

	if (inFile)
	{
		// Read the RAW bytes
		inFile.read((char*)&in[0], (std::streamsize)in.size());

		// Done with file.
		inFile.close();
	}

	// Copy the array data into a float array and scale it heightmap.resize(heightMapHeight * heightMapWidth, 0);
	mHeightmap.resize(sizeHeightMap, 0);

	for (UINT i = 0; i < sizeHeightMap; ++i)
	{
		mHeightmap[i] = (in[i] / 255.0f) * heightScale;
	}
}

SimpleVertex * Terrain::InitVertices()
{
	mNbVertices = mWidth * mDepth;

	SimpleVertex * vertices = new SimpleVertex[mNbVertices];

	int id = 0;
	int dx = 1,dz = 1;

	int tx = 0, ty = 0;

	for (int row = 0; row < mDepth; row++)
	{
		for (int col = 0; col < mWidth; col++)
		{
			// Init x and z position

			vertices[id].Pos.x = col * dx + (-(mWidth-1) * 0.5);
			vertices[id].Pos.z = -(row * dz) + ((mDepth-1) * 0.5);

			// Init textures coordinates
			vertices[id].TexC.x = tx;
			vertices[id].TexC.y = ty;
			tx = (tx + 1) % 2;

			// Init y position
			vertices[id].Pos.y = mHeightmap[col + row * mWidth];

			id++;
		}
		ty = (ty + 1) % 2;
	}

	return vertices;
}

WORD * Terrain::InitIndices()
{
	int nbFaces = (mWidth - 1) * (mDepth - 1) * 2;

	mMesh->IndexCount = nbFaces * 3;

	WORD * indices = new WORD[mMesh->IndexCount];

	int id = 0;

	for (int row = 0; row < mDepth - 1; row++)
	{
		for (int col = 0; col < mWidth - 1; col++)
		{
			// init first triangle face
			int indice = row * mWidth + col;
			indices[id] = indice;

			id++;

			indice += 1;
			indices[id] = indice;

			id++;

			indice = (row+1) * mWidth + col;
			indices[id] = indice;

			id++;

			// init second triangle face
			indice = (row+1) * mWidth + col;
			indices[id] = indice;

			id++;

			indice = row * mWidth + col + 1;
			indices[id] = indice;

			id++;

			indice = (row + 1) * mWidth + col + 1;
			indices[id] = indice;

			id++;
		}
	}

	int t = id;

	return indices;
}


Terrain::~Terrain()
{
	
}
