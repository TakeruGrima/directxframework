#pragma once
#include "Light.h"

class LightManager
{
public:
	static Light * CreateDiffuseLighting(DirectX::XMFLOAT3 &direction, DirectX::XMFLOAT4 &material
		, DirectX::XMFLOAT4 &light)
	{
		return new Light(direction, material, light);
	}

	static Light * CreateAmbiantLighting(DirectX::XMFLOAT4 &material, DirectX::XMFLOAT4 &light)
	{
		return new Light(material, light);
	}

	static Light * CreateSpecularLighting(DirectX::XMFLOAT4 &material, DirectX::XMFLOAT4 &light, 
		const float &power)
	{
		return new Light(material, light, power);
	}
};

