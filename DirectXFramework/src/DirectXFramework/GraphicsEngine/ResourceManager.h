#pragma once
#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <cassert>
#include <vector>
#include <map>

#include "Graphics.h"
#include "Geometry.h"
#include "Terrain.h"
#include "../../DDSTexture/DDSTextureLoader.h"
#include "../../DirectXFramework/PointerDictionary.h"

class ResourceManager
{
	static ResourceManager * mInstance;

	PointerDictionary<ID3D11ShaderResourceView> mTextures;
	PointerDictionary<Geometry> mGeometries;
	PointerDictionary<MeshData> mMeshs;
	PointerDictionary<Shader> mShaders;
public:
	static const std::wstring TextureRoot;
	static const std::wstring HeightmapRoot;
	static const std::string GeometryRoot;
	static const std::string MeshRoot;
	static const std::wstring ShaderRoot;
public:
	ResourceManager();

	static ResourceManager * Get()
	{
		if (mInstance == nullptr)
			mInstance = new ResourceManager();

		return mInstance;
	}

	static wstring StringToWstring(std::string str);

	static ID3D11ShaderResourceView * LoadTexture(const wchar_t * filePath);

	static Geometry * CreateGeometry(std::wstring name,char * path, Geometry::eDataType dataType,
		const wchar_t * texturePath);
	static Terrain * CreateTerrain(std::wstring name, int width, int depth, const wchar_t * texturePath,
		string heightmapPath, int heightMapWidth, int heightMapHeight, float heightScale);
	static Geometry * GetGeometry(wstring geometryName) { return Get()->mGeometries[geometryName]; }

	static MeshData * LoadMesh(char * fullPath);
	static void AddMesh(MeshData * mesh, std::wstring meshName);

	static Shader * LoadShader(string filePath);

	static void Clear() { delete Get(); }

	~ResourceManager();
private:
	// Texture
	static bool IfTextureExist(std::wstring textureName);
	static ID3D11ShaderResourceView * GetTexture(std::wstring textureName) { return Get()->mTextures[textureName]; }
	static void AddTexture(ID3D11ShaderResourceView * texture, std::wstring textureName);
	void ClearTextures();

	//Geometry
	static bool IfGeometryExist(wstring geometryName);
	static void AddGeometry(Geometry * geometry, wstring geometryName);
	void ClearGeometries();

	//Mesh
	static bool IfMeshExist(std::wstring meshName);
	static MeshData * GetMesh(std::wstring meshName) { return Get()->mMeshs[meshName]; }
	void ClearMeshs();

	//Shader
	static bool IfShaderExist(std::wstring shaderName);
	static Shader * GetShader(std::wstring shaderName) { return Get()->mShaders[shaderName]; }
	static void AddShader(Shader * shader, std::wstring shaderName);
	void ClearShaders();
};
#endif
