#include "Shader.h"


Shader::Shader(WCHAR * shaderFileName, D3D11_INPUT_ELEMENT_DESC layout[])
{
	mPPixelShader = nullptr;
	mPVertexShader = nullptr;
	mPVertexLayout = nullptr;

	InitShader(shaderFileName,layout);
}

HRESULT Shader::InitShader(WCHAR * shaderFileName, D3D11_INPUT_ELEMENT_DESC layout[])
{
	HRESULT hr;

	hr = InitVertexShader(shaderFileName,layout);

	if (FAILED(hr))
		return hr;

	hr = InitPixelShader(shaderFileName);

	if (FAILED(hr))
		return hr;
}

HRESULT Shader::InitVertexShader(WCHAR * shaderFileName, D3D11_INPUT_ELEMENT_DESC layout[])
{
	HRESULT hr;

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	hr = CompileShaderFromFile(shaderFileName, "VS", "vs_4_0", &pVSBlob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	// Create the vertex shader
	hr = Graphics::Get3DDevice()->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &mPVertexShader);

	if (FAILED(hr))
		return hr;

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout2[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	UINT numElements = ARRAYSIZE(layout2);

	// Create the input layout
	hr = Graphics::Get3DDevice()->CreateInputLayout(layout2, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &mPVertexLayout);
	pVSBlob->Release();

	if (FAILED(hr))
		return hr;

	// Set the input layout
	Graphics::GetDeviceContext()->IASetInputLayout(mPVertexLayout);

	return hr;
}

HRESULT Shader::InitPixelShader(WCHAR * shaderFileName)
{
	HRESULT hr;

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	hr = CompileShaderFromFile(shaderFileName, "PS", "ps_4_0", &pPSBlob);

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	// Create the pixel shader
	hr = Graphics::Get3DDevice()->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &mPPixelShader);
	pPSBlob->Release();

	if (FAILED(hr))
		return hr;
}

HRESULT Shader::CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* pErrorBlob;
	hr = D3DCompileFromFile(szFileName, nullptr, nullptr, szEntryPoint, szShaderModel,
		dwShaderFlags, 0, ppBlobOut, &pErrorBlob);

	if (FAILED(hr))
	{
		if (pErrorBlob != nullptr)
			OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());

		if (pErrorBlob) pErrorBlob->Release();

		return hr;
	}

	if (pErrorBlob) pErrorBlob->Release();

	return S_OK;
}

Shader::~Shader()
{
	if (mPVertexShader) mPVertexShader->Release();
	if (mPPixelShader) mPPixelShader->Release();
	if (mPVertexLayout) mPVertexLayout->Release();
}
