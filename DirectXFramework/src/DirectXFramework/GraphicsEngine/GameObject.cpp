#include "GameObject.h"

GameObject::GameObject(Geometry * geometry, Shader * shader, XMFLOAT3 &position, XMFLOAT3 &rotation, XMFLOAT3 &scale)
{
	mGeometry = geometry;
	mShader = shader;

	// init world matrix
	XMStoreFloat4x4(&mWorld, XMMatrixIdentity());

	mPosition = position;
	mRotation = rotation;
	mScale = scale;

	mTestTransfo = XMMatrixIdentity();

	mCurrentTransfo = XMMatrixScaling(mScale.x, mScale.y, mScale.z) * XMMatrixRotationX(mRotation.x)
		* XMMatrixRotationY(mRotation.y) * XMMatrixRotationZ(mRotation.z)
		* XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);

	XMStoreFloat4x4(&mWorld, mCurrentTransfo);
}

void GameObject::Rotate(float angleX, float angleY, float angleZ, eSpace relativeSpace)
{
	XMVECTOR newRotaton = XMVectorSet(angleX, angleY, angleZ,1);

	if (relativeSpace == eSpace::WORLD)
	{
		XMVECTOR rotation = XMLoadFloat3(&mRotation) + newRotaton;

		XMStoreFloat3(&mRotation, rotation);
	}
	else
	{
		mTestTransfo *= XMMatrixRotationX(angleX) * XMMatrixRotationY(angleY) * XMMatrixRotationZ(angleZ);
	}
}

void GameObject::Translate(float x, float y, float z, eSpace relativeSpace)
{
	mCurrentTranslation.x = x;
	mCurrentTranslation.y = y;
	mCurrentTranslation.z = z;

	if (relativeSpace == eSpace::WORLD)
	{
		XMVECTOR translation = XMLoadFloat3(&mCurrentTranslation);
		XMVECTOR pos = XMLoadFloat3(&mPosition) + translation;

		XMStoreFloat3(&mPosition, pos);
	}
	else
	{
		mTestTransfo *= XMMatrixTranslation(x,y,z);
	}
}

void GameObject::Update()
{
	mCurrentTransfo = XMMatrixScaling(mScale.x, mScale.y, mScale.z) * XMMatrixRotationX(mRotation.x)
		* XMMatrixRotationY(mRotation.y) * XMMatrixRotationZ(mRotation.z)
		* XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z) * mTestTransfo;

	XMStoreFloat4x4(&mWorld, mCurrentTransfo);
}

void GameObject::Render(Camera * camera, std::vector<Light *> lights)
{
	if(mGeometry != nullptr)
		mGeometry->Render(camera, mWorld, mShader,lights);
}

GameObject::~GameObject()
{

}
