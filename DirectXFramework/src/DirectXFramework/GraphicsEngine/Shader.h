#pragma once

#include <windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>

#include "Graphics.h"

class Shader
{
private:
	ID3D11VertexShader* mPVertexShader;
	ID3D11PixelShader*  mPPixelShader;

	ID3D11InputLayout*  mPVertexLayout;
private:
	HRESULT InitShader(WCHAR * shaderFileName, D3D11_INPUT_ELEMENT_DESC layout[]);
	HRESULT InitVertexShader(WCHAR * shaderFileName, D3D11_INPUT_ELEMENT_DESC layout[]);
	HRESULT InitPixelShader(WCHAR * shaderFileName);
	HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
public:
	Shader(WCHAR * shaderFileName,D3D11_INPUT_ELEMENT_DESC layout[]);

	ID3D11VertexShader * GetVertexShader() { return mPVertexShader; }
	ID3D11PixelShader * GetPixelShader() { return mPPixelShader; }

	~Shader();
};

