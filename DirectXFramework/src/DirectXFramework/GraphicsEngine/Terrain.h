#pragma once

#include <string>
#include <fstream>

#include "Geometry.h"

class Terrain : public Geometry
{
	int mWidth;
	int mDepth;

	std::vector<float> mHeightmap;
protected:
	SimpleVertex * InitVertices();
	WORD * InitIndices();
public:
	Terrain(int width,int depth,const wchar_t * texturePath, std::string heightmapPath,
		int heightMapWidth,int heightMapHeight,float heightScale);

	void LoadHeightmap(std::string heightmapPath,int heightMapWidth, int heightMapHeight, 
		float heightScale);

	~Terrain();
};

