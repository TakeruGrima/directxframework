#include "Geometry.h"
#include "ResourceManager.h"

using namespace DirectX;
using namespace tinyxml2;

Geometry::Geometry()
{
	mMesh = new MeshData();
	mMesh->IndexBuffer = nullptr;
	mMesh->VertexBuffer = nullptr;
	mPTextureRV = nullptr;
}

Geometry::Geometry(char * path, eDataType dataType, const wchar_t * texturePath):Geometry()
{
	string fileName(path);
	if (dataType == XML)
	{
		LoadXMLGeometry(fileName, texturePath);
	}
	else
	{
		LoadMeshGeometry(fileName, texturePath);
	}
}

void Geometry::LoadXMLGeometry(string path, const wchar_t * texturePath)
{
	string fullPath = ResourceManager::GeometryRoot + path;

	MeshData * tmp = ResourceManager::LoadMesh((char*)fullPath.c_str());

	if (tmp == nullptr)
	{
		//Load xmlFile
		mXmlDoc.LoadFile(fullPath.c_str());
		ResourceManager::AddMesh(mMesh, ResourceManager::StringToWstring(fullPath));

		LoadGeometry(texturePath);
	}
	else
	{
		mMesh = tmp;
		LoadTexture(texturePath);
	}
}

void Geometry::LoadMeshGeometry(string path, const wchar_t * texturePath)
{
	string fullPath = ResourceManager::MeshRoot + path;

	MeshData * tmp = ResourceManager::LoadMesh((char*)fullPath.c_str());

	if (tmp == nullptr)
	{
		*mMesh = OBJLoader::Load((char*)fullPath.c_str(), Graphics::Get3DDevice());
		ResourceManager::AddMesh(mMesh, ResourceManager::StringToWstring(fullPath));
	}
	else
		mMesh = tmp;

	LoadTexture(texturePath);
}

void Geometry::LoadGeometry(const wchar_t * texturePath)
{
	// Load Texture
	LoadTexture(texturePath);

	// Init Mesh
	mMesh->VBOffset = 0;
	mMesh->VBStride = sizeof(SimpleVertex);

	// Load Model
	SimpleVertex * vertices = InitVertices();
	WORD * indices = InitIndices();

	// Init normals

	int nbFaces;
	Face * faces = GetFaces(indices, vertices, nbFaces);

	InitVerticesNormals(vertices, faces, nbFaces);
	delete faces;

	// Init buffers
	InitVertexBuffer(vertices);
	InitIndexBuffer(indices);
}

void Geometry::Render(ConstantBuffer cb, Shader * shader)
{
	ID3D11DeviceContext * pContext = Graphics::GetDeviceContext();
	ID3D11Buffer * pConstantBuffer = Graphics::GetConstantBuffer();

	if (mPTextureRV != nullptr)
		pContext->PSSetShaderResources(0, 1, &mPTextureRV);

	pContext->UpdateSubresource(pConstantBuffer, 0, nullptr, &cb, 0, 0);

	// Set vertex buffer
	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;

	pContext->IASetVertexBuffers(0, 1, &mMesh->VertexBuffer,&mMesh->VBStride,&mMesh->VBOffset);

	// Set index buffer

	pContext->IASetIndexBuffer(mMesh->IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	//
	// Renders a triangle
	//
	pContext->VSSetShader(shader->GetVertexShader(), nullptr, 0);
	pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
	pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer);
	pContext->PSSetShader(shader->GetPixelShader(), nullptr, 0);
	pContext->DrawIndexed(mMesh->IndexCount, 0, 0);
}

void Geometry::Render(Camera *camera, XMFLOAT4X4 & world,Shader * shader, std::vector<Light *> lights)
{
	ConstantBuffer cb;
	cb.mWorld = XMMatrixTranspose(XMLoadFloat4x4(&world));
	cb.mView = XMMatrixTranspose(camera->GetViewMatrix());
	cb.mProjection = XMMatrixTranspose(camera->GetProjectionMatrix());

	for (size_t i = 0; i < lights.size(); i++)
	{
		lights[i]->ApplyLighting(&cb, camera);
	}

	Render(cb,shader);
}

Geometry::~Geometry()
{
	/*if (mMesh->VertexBuffer) mMesh.VertexBuffer->Release();
	if (mMesh.IndexBuffer) mMesh.IndexBuffer->Release();*/
}

void Geometry::LoadTexture(const wchar_t * texturePath)
{
	mPTextureRV = ResourceManager::LoadTexture(texturePath);
}

SimpleVertex * Geometry::InitVertices()
{
	XMLElement * verticesElement = mXmlDoc.FirstChildElement("model")->FirstChildElement("vertices");

	mNbVertices = verticesElement->IntAttribute("length");

	SimpleVertex * vertices = new SimpleVertex[mNbVertices];

	XMLElement * curr = verticesElement->FirstChildElement();

	bool end = false;

	for (size_t i = 0; i < mNbVertices; i++)
	{
		SimpleVertex vertex;
		XMLElement * position = curr->FirstChildElement("position");

		float x = position->FloatAttribute("x");
		float y = position->FloatAttribute("y");
		float z = position->FloatAttribute("z");

		vertex.Pos = XMFLOAT3(x, y, z);

		XMLElement * uv = position->NextSiblingElement("uv");

		float u = uv->FloatAttribute("u");
		float v = uv->FloatAttribute("v");

		vertex.TexC = XMFLOAT2(u, v);

		vertices[i] = vertex;

		curr = curr->NextSiblingElement();
	}

	return vertices;
}

HRESULT Geometry::InitVertexBuffer(SimpleVertex * vertices)
{
	HRESULT hr;

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * mNbVertices;// define size buffer
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;

	hr = Graphics::Get3DDevice()->CreateBuffer(&bd, &InitData,&mMesh->VertexBuffer);

	delete vertices;

	if (FAILED(hr))
		return hr;

	return S_OK;
}

WORD * Geometry::InitIndices()
{
	XMLElement * indicesElement = mXmlDoc.FirstChildElement("model")->FirstChildElement("indices");

	int nbIndices = indicesElement->IntAttribute("length");

	mMesh->IndexCount = nbIndices;

	WORD * indices = new WORD[nbIndices];

	std::string indicesString = indicesElement->GetText();

	indicesString.erase(std::remove(indicesString.begin(), indicesString.end(), '\t'), indicesString.end());
	indicesString.erase(std::remove(indicesString.begin(), indicesString.end(), '\n'), indicesString.end());

	int indice = 0;
	int iIndex = 0;

	std::string number;

	for (size_t cpt = 0; cpt < indicesString.size(); cpt++)
	{
		if (indicesString[cpt] == ',')
		{
			int indice = std::stoi(number);
			indices[iIndex] = indice;
			iIndex++;
			number.clear();
		}
		else if(iIndex < nbIndices)
		{
			number.push_back(indicesString[cpt]);
		}
	}

	return indices;
}

HRESULT Geometry::InitIndexBuffer(WORD * indices)
{
	HRESULT hr;

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * mMesh->IndexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = indices;
	hr = Graphics::Get3DDevice()->CreateBuffer(&bd, &InitData,&mMesh->IndexBuffer);

	delete indices;

	if (FAILED(hr))
		return hr;

	return S_OK;
}

Face * Geometry::GetFaces(WORD * indices, SimpleVertex * vertices, int & nbFaces)
{
	nbFaces = mMesh->IndexCount / 3;

	Face * faces = new Face[nbFaces];

	for (size_t facesIndex = 0; facesIndex < nbFaces; facesIndex ++)
	{
		// Indices id
		int id = facesIndex * 3;

		Face face = Face(vertices[indices[id]].Pos, vertices[indices[id +1]].Pos, vertices[indices[id + 2]].Pos);

		XMVECTOR vertice1 = XMLoadFloat3(&face.Vertices[0]);
		XMVECTOR vertice2 = XMLoadFloat3(&face.Vertices[1]);
		XMVECTOR vertice3 = XMLoadFloat3(&face.Vertices[2]);

		//vector u
		XMVECTOR u = vertice2 - vertice1;
		//vector v
		XMVECTOR v = vertice3 - vertice1;

		//normal
		XMVECTOR normal = XMVector3Cross(u, v);
		normal = XMVector3Normalize(normal);

		face.Normal = normal;

		faces[facesIndex] = face;
	}

	return faces;
}

void Geometry::InitVerticesNormals(SimpleVertex * vertices, Face * faces, int nbFaces)
{
	for (size_t vertexID = 0; vertexID < mNbVertices; vertexID++)
	{
		XMVECTOR normal = XMVectorSet(0, 0, 0, 0);
		int nb = 0;
		for (size_t i = 0; i < nbFaces; i++)
		{
			Face currFace = faces[i];

			for (size_t cpt = 0; cpt < 3; cpt++)
			{
				if (Float3IsEqual(currFace.Vertices[cpt], vertices[vertexID].Pos))
				{
					nb++;
					normal += currFace.Normal;
					break;
				}
			}
		}

		XMStoreFloat3(&vertices[vertexID].Normal, normal / nb);
	}
}