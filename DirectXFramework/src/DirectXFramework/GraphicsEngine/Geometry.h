#pragma once
#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <algorithm>
#include <iostream>
#include <sstream>
#include <d3d11_1.h>
#include <directxcolors.h>
#include <vector>

#include "../../TinyXml2/tinyxml2.h"
#include "../../DDSTexture/DDSTextureLoader.h"
#include "../../ObjLoader/OBJLoader.h"

#include "Structures.h"
#include "Graphics.h"
#include "Light.h"
#include "Camera.h"
#include "Shader.h"
#include "../Util.h"

struct Face
{
	XMVECTOR Normal;
	XMFLOAT3 Vertices[3];

	Face()
	{
		Face(DirectX::XMFLOAT3(0, 0, 0), DirectX::XMFLOAT3(0, 0, 0), DirectX::XMFLOAT3(0, 0, 0));
	}

	Face(DirectX::XMFLOAT3 vertex1, DirectX::XMFLOAT3 vertex2, DirectX::XMFLOAT3 vertex3)
	{
		Vertices[0] = vertex1;
		Vertices[1] = vertex2;
		Vertices[2] = vertex3;

		Normal = DirectX::XMVECTOR();
	}
};

class ResourceManager;
class Geometry
{
	tinyxml2::XMLDocument mXmlDoc;
	ID3D11ShaderResourceView * mPTextureRV = nullptr;
protected:
	MeshData * mMesh;
	int mNbVertices;
public:
	enum eDataType {XML,OBJ};
public:
	Geometry();
	Geometry(char * path, eDataType dataType,const wchar_t * texturePath);

	void Render(ConstantBuffer cb,Shader * shader);
	void Render(Camera *camera, XMFLOAT4X4 &world,Shader * shader, std::vector<Light *> lights);

	MeshData * GetMeshData() { return mMesh; }

	~Geometry();
protected:
	void LoadGeometry(const wchar_t * texturePath);
	void LoadTexture(const wchar_t * texturePath);

	void LoadXMLGeometry(std::string path, const wchar_t * texturePath);
	void LoadMeshGeometry(std::string path, const wchar_t * texturePath);
	
	HRESULT InitVertexBuffer(SimpleVertex * vertices);

	virtual SimpleVertex * InitVertices();
	virtual WORD * InitIndices();

	HRESULT InitIndexBuffer(WORD * indices);

	Face * GetFaces(WORD * indices, SimpleVertex * vertices, int &nbFaces);
	void InitVerticesNormals(SimpleVertex * vertices, Face * faces, int nbFaces);
};

#endif

