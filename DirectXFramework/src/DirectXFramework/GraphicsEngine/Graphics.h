#pragma once

#include <d3d11_1.h>
#include <directxcolors.h>

struct ConstantBuffer
{
	DirectX::XMMATRIX mWorld;
	DirectX::XMMATRIX mView;
	DirectX::XMMATRIX mProjection;
	DirectX::XMFLOAT4 DiffuseMtrl;
	DirectX::XMFLOAT4 DiffuseLight;
	DirectX::XMFLOAT3 LightVecW;
	float padding1;

	DirectX::XMFLOAT4 AmbientMtrl;
	DirectX::XMFLOAT4 AmbientLight;

	DirectX::XMFLOAT4 SpecularMtrl;
	DirectX::XMFLOAT4 SpecularLight;
	float SpecularPower;
	DirectX::XMFLOAT3 EyePosW; 	// Camera position in world space
};

class Graphics
{
	static Graphics * mInstance;

	int mWinWidth = 640;
	int mWinHeight = 480;

	ID3D11Device * mPd3dDevice;
	ID3D11DeviceContext* mPImmediateContext;
	ID3D11Buffer* mPConstantBuffer;

	Graphics();
public:
	static Graphics *Get() {
		if (!mInstance)
			mInstance = new Graphics;
		return mInstance;
	}

	static void SetWindowSize(int width, int height) { Get()->mWinWidth = width; Get()->mWinHeight = height; }
	static int GetWindowWidth() { return Get()->mWinWidth; }
	static int GetWindowHeight() { return Get()->mWinHeight; }

	static void Set3DDevice(ID3D11Device * pd3dDevice);
	static void SetDeviceContext(ID3D11DeviceContext* pImmediateContext);
	static void SetConstantBuffer(ID3D11Buffer* pConstantBuffer);

	static ID3D11Device * Get3DDevice() { return Get()->mPd3dDevice; }
	static ID3D11DeviceContext * GetDeviceContext() { return Get()->mPImmediateContext; }
	static ID3D11Buffer * GetConstantBuffer() { return Get()->mPConstantBuffer; }

	~Graphics();
};

