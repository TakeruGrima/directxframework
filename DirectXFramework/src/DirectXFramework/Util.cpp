#include "Util.h"

using namespace DirectX;

bool Float3IsEqual(XMFLOAT3 u, XMFLOAT3 v)
{
	if (u.x == v.x && u.y == v.y && u.z == v.z)
		return true;
	return false;
}