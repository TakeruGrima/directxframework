#pragma once

#include <vector>
#include <cstdlib>
#include <windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include "../../resource.h"

#include "../DirectXFramework/GraphicsEngine/LightManager.h"
#include "../ObjLoader/OBJLoader.h"
#include "../DirectXFramework/GraphicsEngine/Graphics.h"
#include "../DirectXFramework/GraphicsEngine/ResourceManager.h"
#include "../DirectXFramework/GraphicsEngine/Camera.h"
#include "../DirectXFramework/GraphicsEngine/ObjectManager.h"

class D3DApp
{
private:
	HINSTANCE mHInst;
	HWND      mHWnd;
	D3D_DRIVER_TYPE mDriverType;
	D3D_FEATURE_LEVEL mFeatureLevel;

	//Window fields
	wchar_t * mClassName = L"DefaultClass";
	wchar_t * mWindowName = L"Default";

	//Initialise Fields
	UINT mWindowHeight;
	UINT mWindowWidth;

	//Graphics Fields
	float mClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f };

	//Time Fields
	float mPrevTime = 0;

	IDXGISwapChain* mPSwapChain;
	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilView* mDepthStencilView;
	ID3D11RasterizerState* mWireFrame;
	ID3D11RasterizerState* mSolid;
	ID3D11RasterizerState * mCurrentRS;

protected:
	Camera * mCurrentCamera;

	ID3D11RenderTargetView* mPRenderTargetView;
	ID3D11SamplerState * mPSamplerLinear;

	//Lightings
	std::vector<Light *> lights;

public:
	D3DApp();
	~D3DApp();
private:
	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
	HRESULT InitDevice();

	void CleanupAll();
protected:
	void SetWindowClassName(wchar_t * className);
	void SetWindowName(wchar_t * windowName);
	void SetWindowSize(int width, int height);

	void SetClearColor255(int r, int g, int b, int a = 1);
	void SetClearColor(float r, float g, float b, float a = 1);

	virtual void Initialise() {}
	virtual void Cleanup() {}
	virtual void Update(float elapsedTime) {}
	virtual void Draw() {}

	void ClearLights();
public:
	HRESULT InitBase(HINSTANCE hInstance, int nCmdShow);
	void UpdateBase();
	void DrawBase();
};