//#include "Application.h"
//#include "DDSTextureLoader.h"
//
////LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
////{
////	PAINTSTRUCT ps;
////	HDC hdc;
////
////	switch (message)
////	{
////	case WM_PAINT:
////		hdc = BeginPaint(hWnd, &ps);
////		EndPaint(hWnd, &ps);
////		break;
////
////	case WM_DESTROY:
////		PostQuitMessage(0);
////		break;
////
////	default:
////		return DefWindowProc(hWnd, message, wParam, lParam);
////	}
////
////	return 0;
////}
//
//Application::Application()
//{
//	_hInst = nullptr;
//	_hWnd = nullptr;
//	_driverType = D3D_DRIVER_TYPE_NULL;
//	_featureLevel = D3D_FEATURE_LEVEL_11_0;
//	_pd3dDevice = nullptr;
//	_pImmediateContext = nullptr;
//	_pSwapChain = nullptr;
//	_pRenderTargetView = nullptr;
//	_pVertexShader = nullptr;
//	_pPixelShader = nullptr;
//	_pVertexLayout = nullptr;
//	_pVertexBuffer = nullptr;
//	_pIndexBuffer = nullptr;
//	_pConstantBuffer = nullptr;
//}
//
//Application::~Application()
//{
//	Cleanup();
//}
//
//HRESULT Application::Initialise(HINSTANCE hInstance, int nCmdShow)
//{
//	if (FAILED(InitWindow(hInstance, nCmdShow)))
//	{
//		return E_FAIL;
//	}
//
//	RECT rc;
//	GetClientRect(_hWnd, &rc);
//	_WindowWidth = rc.right - rc.left;
//	_WindowHeight = rc.bottom - rc.top;
//
//	if (FAILED(InitDevice()))
//	{
//		Cleanup();
//
//		return E_FAIL;
//	}
//
//	// Initialize the world matrix
//	XMStoreFloat4x4(&_world, XMMatrixIdentity());
//
//	XMStoreFloat4x4(&_world2, XMMatrixIdentity());
//
//	// Initialize the view matrix
//	// camera
//	XMVECTOR Eye = XMVectorSet(0.0f, 0.0f, -3.0f, 0.0f); // camera position
//	XMVECTOR At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);// look at
//	//XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
//
//	mMainCamera = new Camera(Eye, At, Camera::LOOK_AT);
//	cube1 = new Geometry("simpleCube.xml", L"block.dds", _pd3dDevice,_pConstantBuffer);
//	cube2 = new Geometry("simpleCube.xml", L"texture.dds", _pd3dDevice, _pConstantBuffer);
//
//	//XMStoreFloat4x4(&_view, XMMatrixLookAtLH(Eye, At, Up));// Look to for FPS
//
//	// Initialize the projection matrix
//	XMStoreFloat4x4(&_projection, XMMatrixPerspectiveFovLH(XM_PIDIV2, _WindowWidth / (FLOAT)_WindowHeight, 0.01f, 100.0f));// near = 0.01f and far = 100
//
//	return S_OK;
//}
//
//HRESULT Application::InitShadersAndInputLayout()
//{
//	HRESULT hr;
//
//	// Compile the vertex shader
//	ID3DBlob* pVSBlob = nullptr;
//	hr = CompileShaderFromFile(L"DX11 Framework.fx", "VS", "vs_4_0", &pVSBlob);
//
//	if (FAILED(hr))
//	{
//		MessageBox(nullptr,
//			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
//		return hr;
//	}
//
//	// Create the vertex shader
//	hr = _pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &_pVertexShader);
//
//	if (FAILED(hr))
//	{
//		pVSBlob->Release();
//		return hr;
//	}
//
//	// Compile the pixel shader
//	ID3DBlob* pPSBlob = nullptr;
//	hr = CompileShaderFromFile(L"DX11 Framework.fx", "PS", "ps_4_0", &pPSBlob);
//
//	if (FAILED(hr))
//	{
//		MessageBox(nullptr,
//			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
//		return hr;
//	}
//
//	// Create the pixel shader
//	hr = _pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &_pPixelShader);
//	pPSBlob->Release();
//
//	if (FAILED(hr))
//		return hr;
//
//	// Define the input layout
//	D3D11_INPUT_ELEMENT_DESC layout[] =
//	{
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	};
//
//	UINT numElements = ARRAYSIZE(layout);
//
//	// Create the input layout
//	hr = _pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
//		pVSBlob->GetBufferSize(), &_pVertexLayout);
//	pVSBlob->Release();
//
//	if (FAILED(hr))
//		return hr;
//
//	// Set the input layout
//	_pImmediateContext->IASetInputLayout(_pVertexLayout);
//
//	return hr;
//}
//
////HRESULT Application::InitVertexBuffer()
////{
////	HRESULT hr;
////
////	// Create vertex buffer
////	SimpleVertex vertices[] =
////	{
////		{ XMFLOAT3(-1.0f, 1.0f, -1.0f),XMFLOAT3(0.0f, 0.0f, -1.0f),XMFLOAT2(0.0f,0.0f) },
////		{ XMFLOAT3(1.0f, 1.0f, -1.0f),XMFLOAT3(0.0f, 0.0f, -1.0f),XMFLOAT2(1.0f,0.0f) },
////		{ XMFLOAT3(-1.0f, -1.0f, -1.0f),XMFLOAT3(0.0f, 0.0f, -1.0f),XMFLOAT2(0.0f, 1.0f) },
////		{ XMFLOAT3(1.0f, -1.0f, -1.0f),XMFLOAT3(0.0f, 0.0f, -1.0f),XMFLOAT2(1.0f, 1.0f) },
////		{ XMFLOAT3(1.0f, -1.0f, 1.0f),XMFLOAT3(0.0f, 0.0f, 1.0f),XMFLOAT2(0.0f, 1.0f) },
////		{ XMFLOAT3(1.0f, 1.0f, 1.0f),XMFLOAT3(0.0f, 0.0f, 1.0f),XMFLOAT2(0.0f,0.0f) },
////		{ XMFLOAT3(-1.0f, 1.0f, 1.0f),XMFLOAT3(0.0f, 0.0f, 1.0f),XMFLOAT2(1.0f,0.0f) },
////		{ XMFLOAT3(-1.0f, -1.0f, 1.0f),XMFLOAT3(0.0f, 0.0f, 1.0f),XMFLOAT2(1.0f, 1.0f) },
////	};
////
////
////	Geometry test = Geometry("test.xml");
////
////	// if change vertices change this also
////	D3D11_BUFFER_DESC bd;
////	ZeroMemory(&bd, sizeof(bd));
////	bd.Usage = D3D11_USAGE_DEFAULT;
////	bd.ByteWidth = sizeof(SimpleVertex) * 8;// define size buffer
////	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
////	bd.CPUAccessFlags = 0;
////
////	D3D11_SUBRESOURCE_DATA InitData;
////	ZeroMemory(&InitData, sizeof(InitData));
////	InitData.pSysMem = vertices;
////
////	hr = _pd3dDevice->CreateBuffer(&bd, &InitData, &_pVertexBuffer);
////
////	if (FAILED(hr))
////		return hr;
////
////	return S_OK;
////}
//
//HRESULT Application::InitIndexBuffer()
//{
//	HRESULT hr;
//
//	// Create index buffer
//	// if change vertices change there also
//	WORD indices[] =
//	{
//		0,1,2,
//		2,1,3,
//		3,1,4,
//		4,1,5,
//		5,1,6,
//		6,1,0,
//		0,2,7,
//		7,6,0,
//		7,5,6,
//		5,7,4,
//		4,7,3,
//		3,7,2
//	};
//
//	D3D11_BUFFER_DESC bd;
//	ZeroMemory(&bd, sizeof(bd));
//
//	bd.Usage = D3D11_USAGE_DEFAULT;
//	bd.ByteWidth = sizeof(WORD) * _NBINDICE;
//	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	bd.CPUAccessFlags = 0;
//
//	D3D11_SUBRESOURCE_DATA InitData;
//	ZeroMemory(&InitData, sizeof(InitData));
//	//InitData.pSysMem = geometry->GetIndices();
//	hr = _pd3dDevice->CreateBuffer(&bd, &InitData, &_pIndexBuffer);
//
//	if (FAILED(hr))
//		return hr;
//
//	return S_OK;
//}
//
////HRESULT Application::InitPyramidVertexBuffer()
////{
////	HRESULT hr;
////
////	// Create vertex buffer
////	SimpleVertex vertices[] =
////	{
////		{ XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f) },// Tip 0
////		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) },//Top right 1
////		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) },//Top left 2
////		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) },//Bottom right 3
////		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) }//Bottom left 4
////	};
////
////	// if change vertices change this also
////	D3D11_BUFFER_DESC bd;
////	ZeroMemory(&bd, sizeof(bd));
////	bd.Usage = D3D11_USAGE_DEFAULT;
////	bd.ByteWidth = sizeof(SimpleVertex) * 5;// define size buffer
////	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
////	bd.CPUAccessFlags = 0;
////
////	D3D11_SUBRESOURCE_DATA InitData;
////	ZeroMemory(&InitData, sizeof(InitData));
////	InitData.pSysMem = vertices;
////
////	hr = _pd3dDevice->CreateBuffer(&bd, &InitData, &_pPyramidVertexBuffer);
////
////	if (FAILED(hr))
////		return hr;
////
////	return S_OK;
////}
////
////HRESULT Application::InitPyramidIndexBuffer()
////{
////	HRESULT hr;
////
////	// Create index buffer
////	// if change vertices change there also
////	WORD indices[] =
////	{
////		0,2,1,
////		1,3,0,
////		3,4,0,
////		4,2,0,
////		1,4,2,
////		1,3,4
////	};
////
////	D3D11_BUFFER_DESC bd;
////	ZeroMemory(&bd, sizeof(bd));
////
////	bd.Usage = D3D11_USAGE_DEFAULT;
////	bd.ByteWidth = sizeof(WORD) * 18;
////	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
////	bd.CPUAccessFlags = 0;
////
////	D3D11_SUBRESOURCE_DATA InitData;
////	ZeroMemory(&InitData, sizeof(InitData));
////	InitData.pSysMem = indices;
////	hr = _pd3dDevice->CreateBuffer(&bd, &InitData, &_pPyramidIndexBuffer);
////
////	if (FAILED(hr))
////		return hr;
////
////	return S_OK;
////}
////
////HRESULT Application::InitGridVertexBuffer()
////{
////	HRESULT hr;
////
////	// Create vertex buffer
////	SimpleVertex vertices[] =
////	{
////		// Square 1*1
////
////		{ XMFLOAT3(-2.0f, 2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Top Left 0
////		{ XMFLOAT3(-1.0f, 2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Top Right 1
////		{ XMFLOAT3(-2.0f, 1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Left 2
////		{ XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 3
////
////		// Square 1*2
////
////		{ XMFLOAT3(0.0f, 2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Top Right 4
////		{ XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 5
////
////		// Square 1*3
////
////		{ XMFLOAT3(1.0f, 2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Top Right 6
////		{ XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 7
////
////		// Square 1*4
////
////		{ XMFLOAT3(2.0f, 2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Top Right 8
////		{ XMFLOAT3(2.0f, 1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 9
////
////		// Square 2*1
////		// Use 2,3 as top
////		{ XMFLOAT3(-2.0f, 0.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Left 10
////		{ XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 11
////
////		// Square 2*2
////		// Use 3,5 as top and 11 as bottom left
////		{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 12
////
////		// Square 2*3
////		// Use 5,7 as top and 12 as bottom left
////		{ XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 13
////
////		// Square 2*4
////		// Use 7,9 as top and 13 as bottom left
////		{ XMFLOAT3(2.0f, 0.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 14
////
////		// Square 3*1
////		// Use 10,11 as top
////		{ XMFLOAT3(-2.0f, -1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Left 15
////		{ XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 16
////
////		// Square 3*2
////		// Use 11,12 as top and 16 as bottom left
////		{ XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 17
////
////		// Square 3*3
////		// Use 12,13 as top and 17 as bottom left
////		{ XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 18
////
////		// Square 3*4
////		// Use 13,14 as top and 18 as bottom left
////		{ XMFLOAT3(2.0f, -1.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 19
////
////		// Square 4*1
////		// Use 15,16 as top
////		{ XMFLOAT3(-2.0f, -2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Left 20
////		{ XMFLOAT3(-1.0f, -2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 21
////
////		// Square 4*2
////		// Use 16,17 as top and 21 as bottom left
////		{ XMFLOAT3(0.0f, -2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 22
////
////		// Square 4*3
////		// Use 17,18 as top and 22 as bottom left
////		{ XMFLOAT3(1.0f, -2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 23
////
////		// Square 4*4
////		// Use 18,19 as top and 23 as bottom left
////		{ XMFLOAT3(2.0f, -2.0f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },// Bottom Right 24
////	};
////
////	// if change vertices change this also
////	D3D11_BUFFER_DESC bd;
////	ZeroMemory(&bd, sizeof(bd));
////	bd.Usage = D3D11_USAGE_DEFAULT;
////	bd.ByteWidth = sizeof(SimpleVertex) * 25;// define size buffer
////	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
////	bd.CPUAccessFlags = 0;
////
////	D3D11_SUBRESOURCE_DATA InitData;
////	ZeroMemory(&InitData, sizeof(InitData));
////	InitData.pSysMem = vertices;
////
////	hr = _pd3dDevice->CreateBuffer(&bd, &InitData, &_pGridVertexBuffer);
////
////	if (FAILED(hr))
////		return hr;
////
////	return S_OK;
////}
////
////HRESULT Application::InitGridIndexBuffer()
////{
////	HRESULT hr;
////
////	// Create index buffer
////	// if change vertices change there also
////	WORD indices[] =
////	{
////		0,1,2,
////		2,3,1,
////
////		1,4,3,
////		3,5,4,
////
////		4,6,5,
////		5,7,6,
////
////		6,8,7,
////		7,9,8,
////
////		2,3,10,
////		10,11,3,
////
////		3,5,11,
////		11,12,5,
////
////		5,7,12,
////		12,13,7,
////
////		7,9,13,
////		13,14,9,
////
////		10,11,15,
////		15,16,11,
////
////		11,12,16,
////		16,17,12,
////
////		12,13,17,
////		17,18,13,
////
////		13,14,18,
////		18,19,14,
////
////		15,16,20,
////		20,21,16,
////
////		16,17,21,
////		21,22,17,
////
////		17,18,22,
////		22,23,18,
////
////		18,19,23,
////		23,24,19
////	};
////
////	D3D11_BUFFER_DESC bd;
////	ZeroMemory(&bd, sizeof(bd));
////
////	bd.Usage = D3D11_USAGE_DEFAULT;
////	bd.ByteWidth = sizeof(WORD) * _NBINDICEGRID;
////	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
////	bd.CPUAccessFlags = 0;
////
////	D3D11_SUBRESOURCE_DATA InitData;
////	ZeroMemory(&InitData, sizeof(InitData));
////	InitData.pSysMem = indices;
////	hr = _pd3dDevice->CreateBuffer(&bd, &InitData, &_pGridIndexBuffer);
////
////	if (FAILED(hr))
////		return hr;
////
////	return S_OK;
////}
//
////HRESULT Application::InitWindow(HINSTANCE hInstance, int nCmdShow)
////{
////	// Register class
////	WNDCLASSEX wcex;
////	wcex.cbSize = sizeof(WNDCLASSEX);
////	wcex.style = CS_HREDRAW | CS_VREDRAW;
////	wcex.lpfnWndProc = WndProc;
////	wcex.cbClsExtra = 0;
////	wcex.cbWndExtra = 0;
////	wcex.hInstance = hInstance;
////	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_TUTORIAL1);
////	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
////	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
////	wcex.lpszMenuName = nullptr;
////	wcex.lpszClassName = L"TutorialWindowClass";
////	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
////	if (!RegisterClassEx(&wcex))
////		return E_FAIL;
////
////	// Create window
////	_hInst = hInstance;
////	RECT rc = { 0, 0, 640, 480 };
////	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
////	_hWnd = CreateWindow(L"TutorialWindowClass", L"TimotheeLECORRE-1026323", WS_OVERLAPPEDWINDOW,
////		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance,
////		nullptr);
////	if (!_hWnd)
////		return E_FAIL;
////
////	ShowWindow(_hWnd, nCmdShow);
////
////	return S_OK;
////}
//
//HRESULT Application::CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
//{
//	HRESULT hr = S_OK;
//
//	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
//#if defined(DEBUG) || defined(_DEBUG)
//	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
//	// Setting this flag improves the shader debugging experience, but still allows 
//	// the shaders to be optimized and to run exactly the way they will run in 
//	// the release configuration of this program.
//	dwShaderFlags |= D3DCOMPILE_DEBUG;
//#endif
//
//	ID3DBlob* pErrorBlob;
//	hr = D3DCompileFromFile(szFileName, nullptr, nullptr, szEntryPoint, szShaderModel,
//		dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
//
//	if (FAILED(hr))
//	{
//		if (pErrorBlob != nullptr)
//			OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
//
//		if (pErrorBlob) pErrorBlob->Release();
//
//		return hr;
//	}
//
//	if (pErrorBlob) pErrorBlob->Release();
//
//	return S_OK;
//}
//
//HRESULT Application::InitDevice()
//{
//	HRESULT hr = S_OK;
//
//	UINT createDeviceFlags = 0;
//
//#ifdef _DEBUG
//	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
//#endif
//
//	D3D_DRIVER_TYPE driverTypes[] =
//	{
//		D3D_DRIVER_TYPE_HARDWARE,
//		D3D_DRIVER_TYPE_WARP,
//		D3D_DRIVER_TYPE_REFERENCE,
//	};
//
//	UINT numDriverTypes = ARRAYSIZE(driverTypes);
//
//	D3D_FEATURE_LEVEL featureLevels[] =
//	{
//		D3D_FEATURE_LEVEL_11_0,
//		D3D_FEATURE_LEVEL_10_1,
//		D3D_FEATURE_LEVEL_10_0,
//	};
//
//	UINT numFeatureLevels = ARRAYSIZE(featureLevels);
//
//	DXGI_SWAP_CHAIN_DESC sd;
//	ZeroMemory(&sd, sizeof(sd));
//	sd.BufferCount = 1;
//	sd.BufferDesc.Width = _WindowWidth;
//	sd.BufferDesc.Height = _WindowHeight;
//	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;// how many bit / pixel
//	sd.BufferDesc.RefreshRate.Numerator = 60;
//	sd.BufferDesc.RefreshRate.Denominator = 1;
//	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
//	sd.OutputWindow = _hWnd;
//	sd.SampleDesc.Count = 1;
//	sd.SampleDesc.Quality = 0;
//	sd.Windowed = TRUE;
//
//	//Create device
//	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
//	{
//		_driverType = driverTypes[driverTypeIndex];
//		hr = D3D11CreateDeviceAndSwapChain(nullptr, _driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
//			D3D11_SDK_VERSION, &sd, &_pSwapChain, &_pd3dDevice, &_featureLevel, &_pImmediateContext);
//		if (SUCCEEDED(hr))
//			break;
//	}
//
//	if (FAILED(hr))
//		return hr;
//
//	// setup depth stencil here
//
//	// Define buffer
//	D3D11_TEXTURE2D_DESC depthStencilDesc;
//
//	depthStencilDesc.Width = _WindowWidth;
//	depthStencilDesc.Height = _WindowHeight;
//	depthStencilDesc.MipLevels = 1;
//	depthStencilDesc.ArraySize = 1;
//	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;// 24 for depth and 8 for stencil
//	depthStencilDesc.SampleDesc.Count = 1;
//	depthStencilDesc.SampleDesc.Quality = 0;
//	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
//	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
//	depthStencilDesc.CPUAccessFlags = 0;
//	depthStencilDesc.MiscFlags = 0;
//
//	//create depthStencil buffer
//	_pd3dDevice->CreateTexture2D(&depthStencilDesc, nullptr, &_depthStencilBuffer);
//	// create depthStencil view
//	_pd3dDevice->CreateDepthStencilView(_depthStencilBuffer, nullptr, &_depthStencilView);
//
//	// Create a render target view
//	ID3D11Texture2D* pBackBuffer = nullptr;
//	hr = _pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
//
//	if (FAILED(hr))
//		return hr;
//
//	hr = _pd3dDevice->CreateRenderTargetView(pBackBuffer, nullptr, &_pRenderTargetView);
//	pBackBuffer->Release();
//
//	if (FAILED(hr))
//		return hr;
//
//	// replace null_ptr by _depthStencilView because we have one
//	_pImmediateContext->OMSetRenderTargets(1, &_pRenderTargetView, _depthStencilView);
//
//	// Setup the viewport
//	D3D11_VIEWPORT vp;
//	vp.Width = (FLOAT)_WindowWidth;
//	vp.Height = (FLOAT)_WindowHeight;
//	vp.MinDepth = 0.0f;
//	vp.MaxDepth = 1.0f;
//	vp.TopLeftX = 0;
//	vp.TopLeftY = 0;
//	_pImmediateContext->RSSetViewports(1, &vp);
//
//	// Light direction from surface (XYZ)
//	lightDirection = XMFLOAT3(0.25f, 0.5f, -1.0f);
//	// Diffuse material properties (RGBA)
//	diffuseMaterial = XMFLOAT4(0.8f, 0.5f, 0.5f, 1.0f);
//	// Diffuse light colour (RGBA)
//	diffuseLight = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
//
//	//InitShadersAndInputLayout();
//
//	tpShader = new Shader(L"DX11 Framework.fx", nullptr, _pd3dDevice, _pImmediateContext);
//
//	//InitVertexBuffer();
//	//InitIndexBuffer();
//
//	CreateDDSTextureFromFile(_pd3dDevice, L"texture.dds", nullptr, &_pTextureRV);
//
//	//_pImmediateContext->PSSetShaderResources(0, 0, nullptr);
//
//	// Create the sample state
//	D3D11_SAMPLER_DESC sampDesc;
//	ZeroMemory(&sampDesc, sizeof(sampDesc));
//	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
//	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
//	sampDesc.MinLOD = 0;
//	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
//
//	_pd3dDevice->CreateSamplerState(&sampDesc, &_pSamplerLinear);
//	_pImmediateContext->PSSetSamplers(0, 1, &_pSamplerLinear);
//
//	/*InitPyramidVertexBuffer();
//	InitPyramidIndexBuffer();
//
//	InitGridVertexBuffer();
//	InitGridIndexBuffer();*/
//
//	// Set primitive topology
//	_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//
//	// Create the constant buffer
//	D3D11_BUFFER_DESC bd;
//	ZeroMemory(&bd, sizeof(bd));
//	bd.Usage = D3D11_USAGE_DEFAULT;
//	bd.ByteWidth = sizeof(ConstantBuffer);
//	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	bd.CPUAccessFlags = 0;
//	hr = _pd3dDevice->CreateBuffer(&bd, nullptr, &_pConstantBuffer);
//
//	//Creating a rasterizer state 
//
//	D3D11_RASTERIZER_DESC wfdesc;
//	ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
//	wfdesc.FillMode = D3D11_FILL_WIREFRAME;
//	wfdesc.CullMode = D3D11_CULL_NONE;
//	hr = _pd3dDevice->CreateRasterizerState(&wfdesc, &_wireFrame);
//
//	ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
//	wfdesc.FillMode = D3D11_FILL_SOLID;
//	wfdesc.CullMode = D3D11_CULL_NONE;
//	hr = _pd3dDevice->CreateRasterizerState(&wfdesc, &_solid);
//
//	_currentRS = _solid;
//
//	_pImmediateContext->RSSetState(_currentRS);
//
//	if (FAILED(hr))
//		return hr;
//
//	return S_OK;
//}
//
//void Application::Cleanup()
//{
//	if (_pImmediateContext) _pImmediateContext->ClearState();
//
//	if (_pConstantBuffer) _pConstantBuffer->Release();
//	if (_pVertexBuffer) _pVertexBuffer->Release();
//	if (_pIndexBuffer) _pIndexBuffer->Release();
//	if (_pVertexLayout) _pVertexLayout->Release();
//	if (_pVertexShader) _pVertexShader->Release();
//	if (_pPixelShader) _pPixelShader->Release();
//	if (_pRenderTargetView) _pRenderTargetView->Release();
//	if (_pSwapChain) _pSwapChain->Release();
//	if (_pImmediateContext) _pImmediateContext->Release();
//	if (_pd3dDevice) _pd3dDevice->Release();
//
//	if (_depthStencilView) _depthStencilView->Release();
//	if (_depthStencilBuffer) _depthStencilBuffer->Release();
//	if (_wireFrame) _wireFrame->Release();
//	if (_pTextureRV) _pTextureRV->Release();
//	if (_pSamplerLinear) _pSamplerLinear->Release();
//	if (_currentRS) _currentRS->Release();
//
//	if (mMainCamera) delete mMainCamera;
//
//	if (cube1) delete cube1;
//	if (cube2) delete cube2;
//	if (tpShader) delete tpShader;
//}
//
//void Application::Update()
//{
//	// Update our time
//	static float t = 0.0f;
//
//	if (_driverType == D3D_DRIVER_TYPE_REFERENCE)
//	{
//		t += (float)XM_PI * 0.0125f;
//	}
//	else
//	{
//		static DWORD dwTimeStart = 0;
//		DWORD dwTimeCur = GetTickCount();
//
//		if (dwTimeStart == 0)
//			dwTimeStart = dwTimeCur;
//
//		t = (dwTimeCur - dwTimeStart) / 1000.0f;
//	}
//
//	// Animate the sun
//
//	XMStoreFloat4x4(cube1->GetWorldMatrix(), XMMatrixRotationX(t) * XMMatrixRotationY(t) * XMMatrixTranslation(0.0f, 0.0f, 4.0f));
//	XMStoreFloat4x4(cube2->GetWorldMatrix(), XMMatrixRotationX(t) * XMMatrixRotationY(t) * XMMatrixTranslation(2.0f, 0.0f, 4.0f));
//	//// Animate planets
//	//XMStoreFloat4x4(&_world2, XMMatrixRotationX(t) * XMMatrixTranslation(6.0f, 0.0f, 3.0f)
//	//	* XMMatrixScaling(0.5f, 0.5f, 0.5f)
//	//	* XMMatrixRotationZ(t));
//
//	//XMStoreFloat4x4(&_world3, XMMatrixTranslation(0.0f, 1.0f, 4.0f));
//
//	//// Animate Moons
//	//XMStoreFloat4x4(&_worldMoon1, XMMatrixTranslation(6.0f, 0.0f, 3.0f)
//	//	* XMMatrixScaling(0.25f, 0.25f, 0.25f)
//	//	* XMMatrixRotationZ(t) * XMMatrixTranslation(2.5f, 0.0f, 0.0f) * XMMatrixRotationZ(t));
//
//	//XMStoreFloat4x4(&_worldMoon2, XMMatrixTranslation(-6.0f, 0.0f, 3.0f)
//	//	* XMMatrixScaling(0.25f, 0.25f, 0.25f)
//	//	* XMMatrixRotationZ(t) * XMMatrixTranslation(-2.5f, 0.0f, 0.0f) * XMMatrixRotationZ(t));
//
//	if (GetAsyncKeyState(VK_UP))
//	{
//		if (_currentRS == _wireFrame)
//			_currentRS = _solid;
//		else
//			_currentRS = _wireFrame;
//
//		_pImmediateContext->RSSetState(_currentRS);
//	}
//}
//
//void Application::Draw()
//{
//	//_pImmediateContext->PSSetShaderResources(0, 1, &_pTextureRV);
//	//
//	// Clear the back buffer
//	//
//	float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // red,green,blue,alpha
//	_pImmediateContext->ClearRenderTargetView(_pRenderTargetView, ClearColor);
//
//	// clear ClearDepthStencilView
//	_pImmediateContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
//
//	XMMATRIX world = XMLoadFloat4x4(cube1->GetWorldMatrix());
//	XMMATRIX world2 = XMLoadFloat4x4(&_world2);
//	XMMATRIX world3 = XMLoadFloat4x4(&_world3);
//	XMMATRIX worldMoon1 = XMLoadFloat4x4(&_worldMoon1);
//	XMMATRIX worldMoon2 = XMLoadFloat4x4(&_worldMoon2);
//	XMMATRIX view = mMainCamera->GetViewMatrix();//XMLoadFloat4x4(&_view);
//	XMMATRIX projection = XMLoadFloat4x4(&_projection);
//	//
//	// Update variables
//	//
//	cube1->Draw(mMainCamera, projection, _pImmediateContext, tpShader);
//	cube2->Draw(mMainCamera, projection, _pImmediateContext, tpShader);
//
//	////
//	//// Renders a triangle
//	////
//
//	//// Set vertex buffer
//	//stride = sizeof(SimpleVertex);
//	//offset = 0;
//	//_pImmediateContext->IASetVertexBuffers(0, 1, &_pPyramidVertexBuffer, &stride, &offset);
//
//	//// Set index buffer
//	//_pImmediateContext->IASetIndexBuffer(_pPyramidIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
//
//	//// change mWord of ConstantBuffer
//	//cb.mWorld = XMMatrixTranspose(world2);
//
//	//// update the constant buffer
//	//_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, nullptr, &cb, 0, 0);
//
//	//// draw the same cube
//	//_pImmediateContext->DrawIndexed(18, 0, 0);    //change if the number of index here too    
//
//	//													 // change mWord of ConstantBuffer
//
//	//													 // Set vertex buffer
//	//stride = sizeof(SimpleVertex);
//	//offset = 0;
//	//_pImmediateContext->IASetVertexBuffers(0, 1, &_pGridVertexBuffer, &stride, &offset);
//
//	//// Set index buffer
//	//_pImmediateContext->IASetIndexBuffer(_pGridIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
//
//	//cb.mWorld = XMMatrixTranspose(world3);
//
//	//// update the constant buffer
//	//_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, nullptr, &cb, 0, 0);
//
//	//// draw the same cube
//	//_pImmediateContext->DrawIndexed(_NBINDICEGRID, 0, 0);    //change if the number of index here too    
//
//	//// Set vertex buffer
//	//stride = sizeof(SimpleVertex);
//	//offset = 0;
//	//_pImmediateContext->IASetVertexBuffers(0, 1, &_pVertexBuffer, &stride, &offset);
//
//	//// Set index buffer
//	//_pImmediateContext->IASetIndexBuffer(_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
//
//	//cb.mWorld = XMMatrixTranspose(worldMoon1);
//
//	//// update the constant buffer
//	//_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, nullptr, &cb, 0, 0);
//
//	//// draw the same cube
//	//_pImmediateContext->DrawIndexed(_NBINDICE, 0, 0);    //change if the number of index here too    
//
//	//cb.mWorld = XMMatrixTranspose(worldMoon2);
//
//	//// update the constant buffer
//	//_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, nullptr, &cb, 0, 0);
//
//	//// draw the same cube
//	//_pImmediateContext->DrawIndexed(_NBINDICE, 0, 0);    //change if the number of index here too    
//
//	//
//	// Present our back buffer to our front buffer
//	//
//	_pSwapChain->Present(0, 0);
//}