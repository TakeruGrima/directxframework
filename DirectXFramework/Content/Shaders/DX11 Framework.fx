//--------------------------------------------------------------------------------------
// File: DX11 Framework.fx
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 DiffuseMtrl;//describes the diffuse material
	float4 DiffuseLight;//describes the diffuse light colour
	float3 LightVecW;//points in the direction of the light source
	float padding1;

	float4 AmbientMtrl;// describes the ambiant material
	float4 AmbientLight;//describes the ambiant light colour

	float4 SpecularMtrl;
	float4 SpecularLight;
	float SpecularPower;
	float3 EyePosW; 	// Camera position in world space
}

//--------------------------------------------------------------------------------------

struct VS_INPUT
{
	float4 Pos : POSITION;
	float3 Norm : NORMAL;
	float2 Tex : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float3 Norm : NORMAL;
	float3 PosW : POSITION;
	float2 Tex : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float3 Norm : NORMAL;
	float3 PosW : POSITION;
	float2 Tex : TEXCOORD0;
};

//------------------------------------------------------------------------------------
// Vertex Shader - Implements Gouraud Shading using Diffuse lighting only
//------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Pos = mul(input.Pos, World);
	output.PosW = output.Pos;

	// Apply View and Projection transformations

	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);

	// Convert normal from local space to world space

	float3 normalW = mul(float4(input.Norm, 0.0f), World).xyz;
	normalW = normalize(normalW);

	output.Norm = normalW;
	output.Tex = input.Tex;

	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	// Compute the vector from the vertex to the eye position.
	// output.Pos is currently the position in world space
	float3 toEye = normalize(EyePosW - input.PosW.xyz);

	// Compute Colour
	// Compute the reflection vector.
	float3 lightVec = normalize(LightVecW);

	float3 normalW = normalize(input.Norm);

	float3 r = reflect(-lightVec, normalW);

	// Determine how much (if any) specular light makes it
	// into the eye.
	float specularAmount = pow(max(dot(r, toEye), 0.0f), SpecularPower);//(max(v.r, 0))^p
	// v = view Vector = toEye
	// r = reflection of the light
	// dot(r,toEye) => cos angle between r and toEye
	// pow shrink the reflection cone

	// Calculate Diffuse and Ambient Lighting


	float diffuseAmount = max(dot(lightVec, normalW), 0.0f);
	float3 diffuse = diffuseAmount * (DiffuseMtrl * DiffuseLight).rgb;
	float3 ambient = (AmbientMtrl * AmbientLight).rgb;

	// Compute the ambient, diffuse, and specular terms separately.
	float3 specular = specularAmount * (SpecularMtrl * SpecularLight).rgb;

	float4 textureColour = txDiffuse.Sample(samLinear, input.Tex);

	float4 finalColor;

	finalColor.rgb = (textureColour * (ambient + diffuse)) + specular;
	finalColor.a = DiffuseMtrl.a;

	return finalColor;
}
