//#pragma once
//
//#include <windows.h>
//#include <d3d11_1.h>
//#include <d3dcompiler.h>
//#include <directxmath.h>
//#include <directxcolors.h>
//#include "resource.h"
//
//using namespace DirectX;
//
//#include "Camera.h"
//#include "Geometry.h"
//#include "Shader.h"
//
//// change input layer if change the struct
////struct SimpleVertex
////{
////    XMFLOAT3 Pos;
////	XMFLOAT3 Normal;
////	XMFLOAT2 TexC;
////};
//
////struct ConstantBuffer
////{
////	XMMATRIX mWorld;
////	XMMATRIX mView;
////	XMMATRIX mProjection;
////};
//
//class Application
//{
//private:
//	HINSTANCE               _hInst;
//	HWND                    _hWnd;
//	D3D_DRIVER_TYPE         _driverType;
//	D3D_FEATURE_LEVEL       _featureLevel;
//	ID3D11Device*           _pd3dDevice;
//	ID3D11DeviceContext*    _pImmediateContext;
//	IDXGISwapChain*         _pSwapChain;
//	ID3D11RenderTargetView* _pRenderTargetView;
//	ID3D11VertexShader*     _pVertexShader;
//	ID3D11PixelShader*      _pPixelShader;
//	ID3D11InputLayout*      _pVertexLayout;
//
//	ID3D11Buffer*           _pVertexBuffer;
//	ID3D11Buffer*           _pIndexBuffer;
//	ID3D11Buffer*           _pPyramidVertexBuffer;
//	ID3D11Buffer*           _pPyramidIndexBuffer;
//
//	ID3D11Buffer*           _pGridVertexBuffer;
//	ID3D11Buffer*           _pGridIndexBuffer;
//
//	ID3D11Buffer*           _pConstantBuffer;
//	XMFLOAT4X4              _world, _world2, _world3,_worldMoon1,_worldMoon2;// each object have it's own world matrix
//	
//	// Camera
//	Camera * mMainCamera;
//	XMFLOAT4X4              _view;
//
//	XMFLOAT4X4              _projection;
//
//	ID3D11DepthStencilView* _depthStencilView;
//	ID3D11Texture2D* _depthStencilBuffer;
//	ID3D11RasterizerState* _wireFrame;
//	ID3D11RasterizerState* _solid;
//	ID3D11RasterizerState * _currentRS;
//
//	const int _NBINDICE = 36;
//
//	const int _NBINDICEGRID = 96;
//
//	// Light
//	XMFLOAT3 lightDirection;
//	XMFLOAT4 diffuseMaterial;
//	XMFLOAT4 diffuseLight;
//
//	ID3D11ShaderResourceView * _pTextureRV = nullptr;
//	ID3D11SamplerState * _pSamplerLinear = nullptr;
//
//	Geometry * cube1;
//	Geometry * cube2;
//
//	Shader * tpShader;
//
//private:
//	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
//	HRESULT InitDevice();
//	void Cleanup();
//	HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
//	HRESULT InitShadersAndInputLayout();
//	HRESULT InitVertexBuffer();
//	HRESULT InitIndexBuffer();
//
//	HRESULT InitPyramidVertexBuffer();
//	HRESULT InitPyramidIndexBuffer();a
//
//	HRESULT InitGridVertexBuffer();
//	HRESULT InitGridIndexBuffer();
//
//	UINT _WindowHeight;
//	UINT _WindowWidth;
//
//public:
//	Application();
//	~Application();
//
//	HRESULT Initialise(HINSTANCE hInstance, int nCmdShow);
//
//	void Update();
//	void Draw();
//};
//
